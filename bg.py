from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *
from scipy import optimize
import time

def f_Puv(x, overall_density):
    return np.exp(x+x)/(1+np.exp(x+x))-overall_density

def connecting_prob_single_sg(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing the current connecting probability of edges within the subgroup'''
    sg_P = 0

    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)

    for i in lambda_dict.keys():
        com_members_ids = []
        com_members = list(set(lambda_dict[i])&set(sg_nodes))
        num_com_members = len(com_members)

        if com_members != []:
            com_members_ids = np.asarray([sg_nodes.index(j) for j in com_members])
            lambda_M = np.ones((sg_n, sg_n))
            lambda_M[com_members_ids[:,None],com_members_ids]=np.exp(i)

            odds_M = P_M * lambda_M
            P_M = odds_M / (1. - P_M + odds_M)

        else:
            P_M = P_M

    return P_M

def connecting_prob_bi_sgs(sg1_nodes, sg2_nodes, sg1_x_rows, sg2_x_cols, lambda_dict, ud):
    '''
    Computing the current connecting probability of edges
    between two subgroups
    '''
    sg1_N = len(sg1_nodes)
    sg2_N = len(sg2_nodes)

    # finding the common members in sg1_nodes and sg2_nodes
    com_nodes = list(set(sg1_nodes)&set(sg2_nodes))
    com_N = len(com_nodes)

    odds_M = sg1_x_rows[:, None] + sg2_x_cols
    P_M = np.exp(odds_M)/(1. + np.exp(odds_M))

    # The probability of self-edge is 0
    for i in com_nodes:
        P_M[sg1_nodes.index(i), sg2_nodes.index(i)] = 0.

    for i in lambda_dict.keys():
        com_members1 = list(set(lambda_dict[i][0])&set(sg1_nodes))
        com_members2 = list(set(lambda_dict[i][1])&set(sg2_nodes))

        if com_members1 != [] and com_members2 != [] :
            com_members_ids1 = np.asarray([sg1_nodes.index(j) for j in com_members1])
            com_members_ids2 = np.asarray([sg2_nodes.index(j) for j in com_members2])

            lambda_M = np.ones((sg1_N, sg2_N))
            lambda_M[com_members_ids1[:,None],com_members_ids2]=np.exp(i)

            odds_M = P_M * lambda_M
        else:
            odds_M = P_M

        if ud:
            com_members1 = list(set(lambda_dict[i][1])&set(sg1_nodes))
            com_members2 = list(set(lambda_dict[i][0])&set(sg2_nodes))

            if com_members1 != [] and com_members2 != [] :
                com_members_ids1 = np.asarray([sg1_nodes.index(j) for j in com_members1])
                com_members_ids2 = np.asarray([sg2_nodes.index(j) for j in com_members2])
                lambda_M = np.ones((sg1_N, sg2_N))
                lambda_M[com_members_ids1[:,None],com_members_ids2]=np.exp(i)
                odds_M = odds_M * lambda_M

        P_M = odds_M / (1. - P_M + odds_M)

    return P_M

def f_constraint_single_sg(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K

def f_constraint_bi_sgs(x, P_M, sg1_nodes, sg2_nodes, sg1_N, sg2_N, sg_K, ud):
    '''Computing the lambda'''
    lambda_M = np.exp(x)*np.ones((sg1_N,sg2_N))
    update_odds_M = P_M * lambda_M

    if ud:
        com_members = list(set(sg1_nodes)&set(sg2_nodes))

        if com_members != [] :
            com_members_ids1 = np.asarray([sg1_nodes.index(j) for j in com_members])
            com_members_ids2 = np.asarray([sg2_nodes.index(j) for j in com_members])

            lambda_M = np.ones((sg1_N, sg2_N))
            lambda_M[com_members_ids1[:,None],com_members_ids2]=np.exp(x)

            update_odds_M = update_odds_M * lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)
    sg_P = np.sum(P_M)

    return sg_P-sg_K

def initial_bg(A, ud, prior):
    '''Computing the initial background distribution'''
    initial_bg_paras = {}
    if prior == 'individual':
        bd_graph = BGDistr(A, datasource='custom')

        bg_time_s = time.time()
        x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(iterations=1000,\
        verbose=True,undirected=ud,is_square_adj=False)
        bg_time =  time.time() - bg_time_s

        # Check weather probs are right
        all_nodes = list(range(A.shape[0]))
        all_x_rows = x_rows[rowbeans_index[all_nodes]]
        all_x_cols = x_columns[colbeans_index[all_nodes]]

        lambda_sum = all_x_rows[:, None] + all_x_cols

        P_M = np.exp(lambda_sum)/(1. + np.exp(lambda_sum))
        np.fill_diagonal(P_M, 0.0)  # The probability of self-edge is 0

        adjM = A.todense()
        diff_row = np.sum(P_M,axis=0).squeeze()-np.sum(adjM, axis=0).squeeze()
        diff_col = np.sum(P_M,axis=1).squeeze()-np.sum(adjM, axis=1).squeeze()

        print(diff_row[:,:20])
        print(diff_col[:,:20])

    elif prior == 'uniform':
        overall_density = A.count_nonzero()/(A.shape[0]*(A.shape[0]-1))
        bg_time_s = time.time()
        lamb = optimize.fsolve(f_Puv, 1., args = (overall_density))
        bg_time =  time.time() - bg_time_s

        x_rows = np.asarray(lamb)
        x_columns = np.asarray(lamb)
        rowbeans_index = np.array([0]*(A.shape[0]))
        colbeans_index = np.array([0]*(A.shape[0]))

    else:
        raise ValueError("Invalid prior: ", prior)

    initial_bg_paras['x_rows'] = x_rows
    initial_bg_paras['x_columns'] = x_columns
    initial_bg_paras['rowbeans_index'] = rowbeans_index
    initial_bg_paras['colbeans_index'] = colbeans_index

    return initial_bg_paras, bg_time

def check_convergence_indPrior(initial_bg_paras, A):
    x_rows = initial_bg_paras['x_rows']
    x_columns = initial_bg_paras['x_columns']
    rowbeans_index = initial_bg_paras['rowbeans_index']
    colbeans_index = initial_bg_paras['colbeans_index']

    # Check weather probs are right
    all_nodes = list(range(A.shape[0]))
    all_x_rows = x_rows[rowbeans_index[all_nodes]]
    all_x_cols = x_columns[colbeans_index[all_nodes]]

    lambda_sum = all_x_rows[:, None] + all_x_cols

    P_M = np.exp(lambda_sum)/(1. + np.exp(lambda_sum))

    # The probability of self-edge is 0
    np.fill_diagonal(P_M, 0.0)

    A = A.todense()

    diff_row = np.sum(P_M,axis=0).squeeze()-np.sum(A, axis=0).squeeze()
    diff_col = np.sum(P_M,axis=1).squeeze()-np.sum(A, axis=1).squeeze()

    print(diff_row[:,:20])
    print(diff_col[:,:20])

def update_bg_single_sg(top_sg, data, A, lambda_dict):
    '''Updating the background distribution for single subgroup case'''
    _, sg = top_sg
    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)

    P_M = connecting_prob_single_sg(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)
    Ids = np.asarray(sg_nodes)
    sg_K = A[Ids][:,Ids].count_nonzero()

    end_base = 50

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint_single_sg, -end_base, end_base, args = (P_M, sg_n, sg_K))
        end_base += 20.
    else:
        new_lambda = optimize.newton(f_constraint_single_sg, 5., args = (P_M, sg_n, sg_K))

    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    return lambda_dict

def update_bg_bi_sg(top_sg, data, A, lambda_dict, ud):
    '''Updating the background distribution for single subgroup case'''
    _, sg1, sg2 = top_sg
    sg1_nodes, sg1_x_rows, sg1_x_cols = sg1.get_lambdas(data, weightingAttribute=None)
    sg2_nodes, sg2_x_rows, sg2_x_cols = sg2.get_lambdas(data, weightingAttribute=None)

    sg1_N = len(sg1_nodes)
    sg2_N = len(sg2_nodes)

    P_M = connecting_prob_bi_sgs(sg1_nodes, sg2_nodes, sg1_x_rows, sg2_x_cols, lambda_dict, ud)
    R = np.asarray(sg1_nodes)
    C = np.asarray(sg2_nodes)

    # The number of edges between sg1 and sg2
    sg_K = A[R][:,C].count_nonzero()   # rows point to cols
    end_base = 40

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint_bi_sgs, -end_base, end_base, args = (P_M, sg1_nodes, sg2_nodes, sg1_N, sg2_N, sg_K, ud))
        # new_lambda = optimize.newton(f_constraint, 10.)
        end_base += 20.
        # print(f_constraint(new_lambda))
    else:
        new_lambda = optimize.newton(f_constraint_bi_sgs, 5., args = (P_M, sg1_nodes, sg2_nodes, sg1_N, sg2_N, sg_K, ud),maxiter=500)

    if new_lambda != 0.:
        lambda_dict[new_lambda] = [sg1_nodes,sg2_nodes]

    return lambda_dict
