import os

dataset = input('Which dataset? \n \
The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) \
Citeseer(Type c)')

if dataset == "s":
    os.system("python3 synthe_test.py")
elif dataset == "d":
    os.system("python3 delicious_test.py")
elif dataset == "l":
    os.system("python3 lastfm_test.py")
elif dataset == "c":
    os.system("python3 citeseers_test.py")
else:
    print("Invalid option.")
