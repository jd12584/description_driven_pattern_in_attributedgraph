import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import os

import time
import pickle

from visualisation import *
from get_graph_and_attributes import *


def compute_individual_prior(A, ud):
    bg_dist = BGDistr(A, datasource='custom')

    (x_rows, x_columns, rowbeans_index, colbeans_index, _, _) = \
    bg_dist.compute_lambdas_in_a_cooler_way(iterations=1000, verbose=False,
                                            undirected=ud,
                                            is_square_adj=False)
    return x_rows, x_columns, rowbeans_index, colbeans_index

def f_Puv(x, overall_density):
    return np.exp(x+x)/(1+np.exp(x+x))-overall_density

def compute_uniform_prior(A):
    overall_density = A.count_nonzero()/(A.shape[0]*(A.shape[0]-1))
    lamb = optimize.fsolve(f_Puv, 1., args = (overall_density))

    x_rows = np.asarray(lamb)
    x_columns = np.asarray(lamb)
    rowbeans_index = np.array([0]*(A.shape[0]))
    colbeans_index = np.array([0]*(A.shape[0]))
    return x_rows, x_columns, rowbeans_index, colbeans_index

def Connecting_Prob(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing the current connecting probability of edges within the subgroup'''
    sg_P = 0
    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)

    for i in lambda_dict.keys():
        com_members_ids = []
        com_members = list(set(lambda_dict[i])&set(sg_nodes))
        num_com_members = len(com_members)

        if com_members != []:
            com_members_ids = np.asarray([sg_nodes.index(j) for j in com_members])
            lambda_M = np.ones((sg_n, sg_n))
            lambda_M[com_members_ids[:,None],com_members_ids]=np.exp(i)
            odds_M = P_M * lambda_M
            P_M = odds_M / (1. - P_M + odds_M)

        else:
            P_M = P_M
    return P_M


def f_constraint(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M
    P_M = update_odds_M / (1. - P_M + update_odds_M)
    sg_P = np.sum(P_M)
    return sg_P-sg_K

beamwidths = [1,2,3,4,5,10,20,40]
# beamwidths = [1,2,3,4,5,10,15,20,25,30,35,40]
search_time = []
Results = []

G, data, name = from_lastfm(11900)
ud = True
total_A = nx.adjacency_matrix(G)

# Initial background distribution
x_rows, x_columns,rowbeans_index,colbeans_index = compute_individual_prior(total_A, ud)
# x_rows, x_columns,rowbeans_index,colbeans_index = compute_uniform_prior(total_A)
lambda_dict = {}

# Search space
searchspace = psx.createSelectors(data)
print(searchspace)
print(len(searchspace))

num_itr = input('How many iterations?\
1 (type 1) or 2 (type 2)')

if num_itr == '1':
    cwd = os.getcwd()
    file = os.path.join(cwd, *['results','si_' + name +'single_varyingBw_oneItr.pkl'])

    for w in beamwidths:
        target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict,ud)
        task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[w],depth=2,qf=psx.graph_target.SubjectiveInterestingness())
        search_time_s = time.time()
        result = psx.BeamSearch(beamWidth=w).execute(task)
        search_time.append(time.time() - search_time_s)
        Results.append(result)

        # save the results in each iteration
        Obj = (beamwidths,search_time,Results)
        f = open(file,'wb')
        pickle.dump(Obj, f)
        f.close()

        f = open(file,'rb')
        Obj = pickle.load(f)
        f.close()
        print(Obj)

else:
    cwd = os.getcwd()
    file = os.path.join(cwd, *['results','si_' + name +'single_varyingbw_twoItr.pkl'])

    target = psx.GTarget(G, x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict, ud)
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[1],depth=2,qf=psx.graph_target.SubjectiveInterestingness())
    # search_time_s = time.time()
    result = psx.BeamSearch(beamWidth=1).execute(task)
    # search_time.append(time.time() - search_time_s)
    print(result)

    # one more iteration
    _, sg = result[0]
    end_base = 50.
    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)
    P_M = Connecting_Prob(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)
    Ids = np.asarray(sg_nodes)
    sg_K = target.Adj_M[Ids][:,Ids].count_nonzero()
    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg_n, sg_K))
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg_n, sg_K))
    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    for w in beamwidths:
        target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict,ud)
        task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[w],depth=2,qf=psx.graph_target.SubjectiveInterestingness())
        search_time_s = time.time()
        result = psx.BeamSearch(beamWidth=w).execute(task)
        search_time.append(time.time() - search_time_s)
        Results.append(result)

        print(Results)
        print(search_time)

        # save the results in each iteration
        Obj = (beamwidths,search_time,Results)
        f = open(file,'wb')
        pickle.dump(Obj, f)
        f.close()

        f = open(file,'rb')
        Obj = pickle.load(f)
        f.close()
        print(Obj)
