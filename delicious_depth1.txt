Which dataset? 
 The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) Citeseer(Type c), Caltech36(Type ct), Reed98(Type r)1
[8]
[(8,  28371) (8,  40306) (8,  85279) (8,  91764) (8,  97723) (8, 105468)]
1867
[(8, 28371), (8, 40306), (8, 85279), (8, 91764), (8, 97723), (8, 105468), (8, 106690), (32, 3203), (32, 7978), (32, 8670)]
  (0, 638)	1
  (0, 820)	1
  (0, 1535)	1
  (0, 1620)	1
  (0, 1711)	1
  (0, 1828)	1
  (0, 1849)	1
   design  tools  video    ...      ipodtouch  socialnetworking  animation
0       0      0      0    ...              0                 0          0
1       1      0      0    ...              0                 0          0
2       1      0      1    ...              0                 0          0
3       0      1      1    ...              1                 0          0
4       1      1      1    ...              0                 0          0

[5 rows x 100 columns]
Index(['design', 'tools', 'video', 'education', 'art', 'webdesign', 'web',
       'web20', 'twitter', 'blog', 'reference', 'google', 'technology',
       'inspiration', 'development', 'javascript', 'programming', 'writing',
       'software', 'music', 'socialmedia', 'resources', 'research', 'internet',
       'visualization', 'iphone', 'free', 'viapackratius', 'tutorial', 'css',
       'business', 'howto', 'tips', 'photography', 'mobile', 'ux', 'social',
       'history', 'science', 'facebook', 'data', 'media', 'library',
       'wordpress', 'opensource', 'collaboration', 'webdev', 'culture',
       'games', 'ipad', 'news', 'books', 'typography', 'politics', 'online',
       'journalism', 'flash', 'jquery', 'search', 'marketing', 'digital',
       'html5', 'learning', 'apple', 'community', 'tool', 'food', 'statistics',
       'critical', 'security', 'portfolio', 'interactive', 'blogs', 'mac',
       'math', 'youtube', 'maps', 'graphics', 'tutorials', 'html', 'shopping',
       'python', 'architecture', 'audio', 'ruby', 'funny', 'teaching', 'rules',
       'article', 'apps', 'humor', 'ui', 'usability', 'advertising', 'code',
       'fun', 'database', 'ipodtouch', 'socialnetworking', 'animation'],
      dtype='object')
-------------------------------------------------------------------
iteration0
(65.50996655883088, <<T: Density of connectivity; D: design=1>>, <<T: Density of connectivity; D: design=0>>)
SI = 65.510      D1: design=1      D2: design=0      Num1: 844      Num2: 1023       Num_com: 0      sg_K: 3008      sg_P = 3713.790

(65.50996357505812, <<T: Density of connectivity; D: design=0>>, <<T: Density of connectivity; D: design=1>>)
SI = 65.510      D1: design=0      D2: design=1      Num1: 1023      Num2: 844       Num_com: 0      sg_K: 3008      sg_P = 3713.790

(47.80684565331186, <<T: Density of connectivity; D: javascript=0>>, <<T: Density of connectivity; D: javascript=1>>)
SI = 47.807      D1: javascript=0      D2: javascript=1      Num1: 1409      Num2: 458       Num_com: 0      sg_K: 1937      sg_P = 2423.161

(47.80684456777919, <<T: Density of connectivity; D: javascript=1>>, <<T: Density of connectivity; D: javascript=0>>)
SI = 47.807      D1: javascript=1      D2: javascript=0      Num1: 458      Num2: 1409       Num_com: 0      sg_K: 1937      sg_P = 2423.161

(46.24311426763721, <<T: Density of connectivity; D: art=1>>, <<T: Density of connectivity; D: art=0>>)
SI = 46.243      D1: art=1      D2: art=0      Num1: 560      Num2: 1307       Num_com: 0      sg_K: 2819      sg_P = 3387.643

(46.243110896924996, <<T: Density of connectivity; D: art=0>>, <<T: Density of connectivity; D: art=1>>)
SI = 46.243      D1: art=0      D2: art=1      Num1: 1307      Num2: 560       Num_com: 0      sg_K: 2819      sg_P = 3387.643

