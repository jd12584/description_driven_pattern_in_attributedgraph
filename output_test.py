import pickle
import pysubgroupx as psx
import os
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from get_graph_and_attributes import *
# # ------------------------------------------------------------------------------
#
# f = open('output.txt','rb')
# quality =[]
#
# for x in f:
#     quality.append(float(x))
#
# print(quality[:10])
# quality.sort(reverse=True)
# print(quality[:10])
#
# f.close()
#
# f2 = open('results/bi_Reed98_results.pkl', 'rb')
# Obj = pickle.load(f2)
# f2.close()
#
# for i in range(len(Obj[-1])):
#     print('----------------------------------------')
#     print('iteration' + str(i))
#     print(Obj[-1][i])
#
# # ------------------------------------------------------------------------------


# # ------------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Get a graph and the user-attributes data frame from a particular dataset
# -----------------------------------------------------------------------------
dataset = input('Which dataset? \n \
The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) \
Citeseer(Type c), Caltech36(Type ct), Reed98(Type r), DBLP topics as attributes(Type dbt)\
DBLP countries as attributes(Type dbc)')

if dataset == "s":
    G, data, name = from_old_synthesized_data()
    ud=False
elif dataset == "d":
    G, data, name = from_delicious(100)
    ud=True
elif dataset == "l":
    G, data, name = from_lastfm(11900)
    ud=True
elif dataset == "c":
    G, data, name = from_citeseer()
    ud=False
elif dataset == "ct":
    G, data, name = from_facebook100('Caltech36.mat')
    ud=True
elif dataset == "r":
    G, data, name = from_facebook100('Reed98.mat')
    ud=True
elif dataset == "dbt":
    G, data, name = from_dblp_topics()
    ud=False
elif dataset == "dbc":
    G, data, name = from_dblp_affs()
    ud=False
else:
    print("Invalid option.")

#
# Initial background distribution
#
which_prior = input('Pior beliefs on indiviual vertex degrees  (Type i) \n \
or overall degree ? (Type o)')

if which_prior == 'i':
    name = name + '_individual'
elif which_prior == 'o':
    name = name +'_uniform'
else:
    print("Invalid option.")

# ------------------------------------------------------------------------------
# bi-subgroup case
#
# cwd = os.getcwd()
# file = os.path.join(cwd, *['results','bi_' + name +'_results.pkl'])
#
# f = open(file,'rb')
# Obj = pickle.load(f)
# f.close()
#
# Result = Obj[-1]
# for i in range(len(Result)):
#     print('-------------------------------------------------------------------')
#     print('iteration' + str(i))
#
#     for j in range(14):
#         SI, sg1, sg2 = Result[i][j]
#
#         sg1_instances = sg1.subgroupDescription.covers(data)
#         sg1_nodes = np.where(sg1_instances)[0]
#
#         sg2_instances = sg2.subgroupDescription.covers(data)
#         sg2_nodes = np.where(sg2_instances)[0]
#
#         com_nodes = [i for i in sg1_nodes if i in sg2_nodes]
#         Num_com = len(com_nodes)
#
#
#         qf=psx.SubjectiveInterestingness()
#         # qf=psx.graph_target.SubjectiveInterestingness()
#
#         dict = sg1.target.lambda_dict
#
#         # sg1.target.lambda_dict = {}
#         # sg2.target.lambda_dict = {}
#
#         sg1.target.lambda_dict = {k: dict[k] for k in list(dict)[:i]}
#         sg2.target.lambda_dict = {k: dict[k] for k in list(dict)[:i]}
#
#         (sg_K, sg_N, sg_P) = qf.computeStatistics_BiCases(data, sg1, sg2)
#         num_attr = sg1.subgroupDescription.__len__() + \
#                 sg2.subgroupDescription.__len__()
#
#         SI_ = qf.computeSI(sg_K, sg_N, sg_P, num_attr)
#         # print(sg2.target.lambda_dict)
#         # print(SI_)
#
#         title = 'SI = ' + '{:.03f}'.format(SI_) + '      D1: ' + str(sg1.subgroupDescription) + \
#         '      D2: ' + str(sg2.subgroupDescription)+ '      Num1: ' + str(len(sg1_nodes))+'      Num2: '\
#          + str(len(sg2_nodes))+ '       Num_com: ' + str(Num_com) + '      sg_K: ' + str(sg_K) +  \
#          '      sg_P = ' + '{:.03f}'.format(sg_P)
#
#         print(Result[i][j])
#         print(title)
#         print('')
#
#         # cwd = os.getcwd()
#         # file = os.path.join(cwd, *['results','bi_' + name,'216.pdf'])
#         #
#         # if dataset == "s":
#         #     indicate_bicluster_in_smallNetwork(G, list(sg1_nodes), list(sg2_nodes), file, title)
#         # else:
#         #     indicate_bicluster_in_realNetwork(G, list(sg1_nodes), list(sg2_nodes), file, title)
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# single subgroup case for SI
cwd = os.getcwd()
file = os.path.join(cwd, *['results','si_' + name +'_results.pkl'])

f = open(file,'rb')
Obj = pickle.load(f)
f.close()

Result = Obj[-1]

for i in range(len(Result)):
    print('-------------------------------------------------------------------')
    print('iteration' + str(i))

    for j in range(10):
        SI, sg = Result[i][j]

        sg_instances = sg.subgroupDescription.covers(data)
        sg_nodes = np.where(sg_instances)[0]

        qf=psx.graph_target.SubjectiveInterestingness()

        dict = sg.target.lambda_dict

        sg.target.lambda_dict = {k: dict[k] for k in list(dict)[:i]}

        (sg_K, sg_N, sg_P) = sg.target. get_base_statistics(data, sg)
        num_attr = sg.subgroupDescription.__len__()

        SI_ = qf.computeSI(sg_K, sg_N, sg_P, num_attr)
        # print(sg2.target.lambda_dict)
        # print(SI_)
        Ids = np.asarray(sg_nodes)
        Rows = sg.target.Adj_M[Ids]

        Num_interEdges = Rows.count_nonzero() - sg_K

        title = 'SI = ' + '{:.03f}'.format(SI_) + '      D1: ' + str(sg.subgroupDescription) + \
        '      Num: ' + str(len(sg_nodes))+'      sg_K: ' + str(sg_K/2.) +  \
         '      sg_P = ' + '{:.03f}'.format(sg_P/2.) + '      Num_interEdges = ' + '{:.03f}'.format(Num_interEdges)

        print(Result[i][j])
        print(title)
        print('')
# ------------------------------------------------------------------------------


# # # ------------------------------------------------------------------------------
# # # single subgroup case for other measures
#
# # Choosing Measurement
#
# choice = input('IntraCluserDensity (Type 1) \n \
# or Average Degree (Type 2) \n \
# or InverseConductance (Type 3)\n \
# or SimonMeasure (Type 4)\n \
# or SegregationIndex (Type 5)\n \
# or InverseAverageODF (Type 6)\n \
# or LocalModularity (Type 7)\n \
# or FractionOverMedianDegree (Type 8)\n \
# or EdgeSurpls (Type 9)?')
#
# if choice == '1':
#     qf=psx.graph_target.IntraClusterDensity()
#     measure = 'EdgeDensity'
#
# elif choice == '2':
#     qf=psx.graph_target.AverageDegree()
#     measure = 'AverageDegree'
#
# elif choice == '3':
#     qf=psx.graph_target.InverseConductance()
#     measure = 'InvConuctance'
#
# elif choice == '4':
#     qf=psx.graph_target.SimonMeasure()
#     measure = 'SimonM'
#
# elif choice == '5':
#     qf=psx.graph_target.SegregationIndex()
#     measure = 'SIDX'
#
# elif choice == '6':
#     qf=psx.graph_target.InverseAverageODF()
#     measure = 'IAODF'
#
# elif choice == '7':
#     qf=psx.graph_target.LocalModularity()
#     measure = 'MODL'
#
# elif choice == '8':
#     qf=psx.graph_target.FractionOverMedianDegree()
#     measure = 'FOMD'
#
# elif choice == '9':
#     qf=psx.graph_target.EdgeSurplus()
#     measure = 'EdgeSurplus'
#
# else:
#     print("Invalid option.")
#
#
# cwd = os.getcwd()
# file = os.path.join(cwd, *['results','comparison','si_' + name + measure+'_results.pkl'])
#
# f = open(file,'rb')
# Obj = pickle.load(f)
# f.close()
#
# Result = Obj[-1]
#
# for i in range(len(Result)):
#     print('-------------------------------------------------------------------')
#     print('iteration' + str(i))
#
#     for j in range(20):
#         S, sg = Result[i][j]
#
#         sg_instances = sg.subgroupDescription.covers(data)
#         sg_nodes = np.where(sg_instances)[0]
#
#         Ids = np.asarray(sg_nodes)
#
#         Rows = sg.target.Adj_M[Ids]
#         Blocks = Rows[:,Ids]
#
#         Num_intraEdges = Blocks.count_nonzero()
#         Num_interEdges = Rows.count_nonzero() - Num_intraEdges
#
#
#         title = measure + ' = ' + '{:.03f}'.format(S) + '      D1: ' + str(sg.subgroupDescription) + \
#         '      Num: ' + str(len(sg_nodes))+'      sg_K: ' + str(Num_intraEdges/2.) +  \
#          '      Num_interEdges = ' + '{:.03f}'.format(Num_interEdges)
#
#         print(Result[i][j])
#         print(title)
#         print('')
# # ------------------------------------------------------------------------------


# # # ------------------------------------------------------------------------------
# # # Scalability by varying NumAttr
# # note no need to call from get_graph_and_attributes
#
# cwd = os.getcwd()
# file = os.path.join(cwd, *['results','For scalability','scalability_lastfm_varyingNumAttr.pkl'])
#
# f = open(file,'rb')
# Obj = pickle.load(f)
# f.close()
#
# print('number of selectors:')
# print([i*2 for i in Obj[0]])
# print('\n')
# print('time:  ')
# print(Obj[1])

# # ------------------------------------------------------------------------------
# #
# #
# # ------------------------------------------------------------------------------
# # Scalability bi subgroup case
# # note no need to call from get_graph_and_attributes
#
# cwd = os.getcwd()
# file = os.path.join(cwd, *['results','For scalability','bicases','bi' +'_lastfm_time_results_1.pkl'])
#
# f = open(file,'rb')
# Obj = pickle.load(f)
# f.close()
#
# print(Obj)
#
# # ------------------------------------------------------------------------------
