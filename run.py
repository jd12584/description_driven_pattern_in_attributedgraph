from workflows import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-w','--work_dir', default='./',
                        help="Directory containing parameter file.")
    parser.add_argument('-c','--cache_dir', default='./cache',
                        help="Directory containing cache files.")
    parser.add_argument('-p','--param_json', default='params.json',
                        help="Name of parameter file.")
    args = parser.parse_args()

    set_logger(join(args.work_dir, 'active_learning.log'))

    params = load_config(args.work_dir, args.param_json)

    if params.workflow == "single_start":
        single_start(args.work_dir, args.cache_dir, params)
    elif params.workflow == "multiple_start":
        multiple_start(args.work_dir, args.cache_dir, params)
    else:
        raise ValueError("Unimplemented workflow: ", params.workflow)
