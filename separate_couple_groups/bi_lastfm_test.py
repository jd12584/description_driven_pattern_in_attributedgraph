import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import os

import time
import pickle

from visualisation import *


cwd = os.getcwd()
file_re = os.path.join(cwd, *['datasets','Lastfm', 'user_friends.dat'])
file_tags = os.path.join(cwd, *['datasets','Lastfm', 'tags.dat'])

file_attr = os.path.join(cwd, *['datasets','Lastfm', 'user_taggedartists.dat'])


# -----------------------------------------------------------------------------
# Read the tag dataset
# -----------------------------------------------------------------------------

data_tag = pd.read_csv(file_tags,
                    sep='\t',
                    encoding="latin1")


tagID = list(data_tag[data_tag.columns[0]])
tagValue = list(data_tag[data_tag.columns[-1]])

tag_mapping = dict(zip(tagID,tagValue))

print(tagID[0])


# -----------------------------------------------------------------------------
# Get all the users
# -----------------------------------------------------------------------------
data_user = pd.read_csv(file_attr,
                sep='\t',
                header=1,
                usecols=[0])

userID = data_user.values
print(userID[0])


# -----------------------------------------------------------------------------
# Construct a grpah correspoinding to the friend relationship dataset
# -----------------------------------------------------------------------------


data_re = np.genfromtxt(file_re,
                     names=True,
                     dtype=None,
                     usecols=(0,1))

print(data_re[0])

G=nx.Graph()

G.add_nodes_from(np.unique(userID))
G.add_edges_from(data_re)

N = len(G)
print(N)

node_mapping=dict(zip(G.nodes(),range(N)))
nx.relabel_nodes(G,node_mapping,copy=False)

print(node_mapping[8])

total_A = nx.adjacency_matrix(G)


#
# # -----------------------------------------------------------------------------
# # Construct the user-attributes data frame
# # -----------------------------------------------------------------------------
#
data_attr = np.genfromtxt(file_attr,
                     names=True,
                     dtype=None,
                     usecols=(0, 2))


# a dicitonary with the keys as the users and values as the tags each user assigned
attr_dict = {node_mapping[k[0]]: set() for k in data_attr}

# a dictionary with the keys as the tags and values as the assignment frequency
# of each tag
attr_freq_dict = dict.fromkeys(tagValue,0)


##
## get the 1350 most frequent tagsprior
##

for k in data_attr:
    attr_dict[node_mapping[k[0]]].add(tag_mapping[k[-1]])
    attr_freq_dict[tag_mapping[k[-1]]] += 1

attrs = nlargest(100, attr_freq_dict, key=attr_freq_dict.__getitem__)

attrs_set = set(attrs)
for k in range(N):

    attr_dict[k]= attr_dict[k].intersection(attrs_set)


mlb = MultiLabelBinarizer(classes=attrs)
attr_rec = mlb.fit_transform(list(attr_dict.values()))
# print(list(mlb.classes_))

for k in range(N):
    attr_dict[k] = list(attr_rec[k,:])

data = pd.DataFrame.from_dict(attr_dict, orient='index',columns=attrs)

print(data.index)
# print(data.head())
print(data.columns)

# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')

bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(reltol=.01,iterations=5000,verbose=False)

bg_time =  time.time() - bg_time_s

lambda_dict = {}


Result = []
#
#
# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------
target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# print(nx.adjacency_matrix(target.graph))

searchspace = psx.createSelectors(data)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[7,3],depth=2,qf=psx.SubjectiveInterestingness())
#
search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute_in_bicases(task)
search_time.append(time.time() - search_time_s)

Result.append(result)
print(result)


total_A = nx.adjacency_matrix(G)


def Connecting_Prob_BiCases(sg1_nodes, sg2_nodes, lambda_dict):
    '''Computing the current connecting probability of edges between two subgroups'''
    sg1_N = len(sg1_nodes)
    sg2_N = len(sg2_nodes)

    # finding the common members in sg1_nodes and sg2_nodes
    com_nodes = [i for i in sg1_nodes if i in sg2_nodes]

    com_N = len(com_nodes)

    R = list(sg1_nodes)*sg2_N
    C = list(np.repeat(sg2_nodes,sg1_N))

    # The lower bound of the number of edges between sg1 and sg2
    sg_K = total_A[R, C].sum()   # rows point to cols

    print(sg_K)

    odds_M = sg1_x_rows[:, None] + sg2_x_cols

    P_M = np.exp(odds_M)/(1. + np.exp(odds_M))

    # The probability of self-edge is 0
    for i in com_nodes:
        P_M[sg1_nodes.index(i), sg2_nodes.index(i)] = 0.

    for i in lambda_dict.keys():
        com_members_ids1 = []

        com_members1 = [j for j in lambda_dict[i][0] if j in sg1_nodes]
        num_com_members1 = len(com_members1)

        com_members2 = [j for j in lambda_dict[i][1] if j in sg2_nodes]
        num_com_members2 = len(com_members2)

        if com_members1 != [] and com_members2 != [] :

            com_members_ids1 = [sg1_nodes.index(j) for j in com_members1]
            com_members_ids2 = [sg2_nodes.index(j) for j in com_members2]

            row = np.asarray(com_members_ids1 * num_com_members2)
            col = np.repeat(com_members_ids2, num_com_members1)

            value = [i]*(num_com_members1*num_com_members2)

            lambda_M = csr_matrix((value,(row,col)), shape=(sg1_N, sg2_N)).toarray()

            new_odds_M = P_M * np.exp(lambda_M)

            P_M = new_odds_M / (1. - P_M + new_odds_M)
        else:
            P_M = P_M

    return (P_M, sg_K)


def f_constraint(x, P_M, sg1_N, sg2_N, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg1_N,sg2_N))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K


# -----------------------------------------------------------------------------
# The sequel -- update the background distribution
# -----------------------------------------------------------------------------

num_it = 1;
end_base = 40.

for it in range(num_it):

    _, sg1, sg2 = result[0]

    sg1_nodes, sg1_x_rows, sg1_x_cols = sg1.get_lambdas(data, weightingAttribute=None)
    sg2_nodes, sg2_x_rows, sg2_x_cols = sg2.get_lambdas(data, weightingAttribute=None)

    print(sg1_nodes)
    print(sg2_nodes)

    (P_M, sg_K) = Connecting_Prob_BiCases(sg1_nodes, sg2_nodes, lambda_dict)

    sg1_N = len(sg1_nodes)
    sg2_N = len(sg2_nodes)

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg1_N, sg2_N, sg_K))
        # new_lambda = optimize.newton(f_constraint, 10.)
        end_base += 20.
        # print(f_constraint(new_lambda))
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg1_N, sg2_N, sg_K))


    if new_lambda != 0.:
        lambda_dict[new_lambda] = [sg1_nodes,sg2_nodes]

    print(lambda_dict)

    target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

    # print(nx.adjacency_matrix(target.graph))
    # searchspace = psx.createSelectors(data)

    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[7,3],depth=2,qf=psx.SubjectiveInterestingness())

    search_time_s = time.time()
    result = psx.BeamSearch(beamWidth=30).execute_in_bicases(task)
    search_time_e = time.time()

    search_time.append(search_time_e - search_time_s)

    Result.append(result)
    # print(lambda_dict)

for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])
#
#
# #
#
# -----------------------------------------------------------------------------
# Save some key variables
# -----------------------------------------------------------------------------

Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
f = open('results/bi_lastfm_results.pkl','wb')
pickle.dump(Obj, f)
f.close()

# f = open('results/bi_lastfm_results.pkl','rb')
# Obj = pickle.load(f)
# f.close()
#
# for i in range(len(Obj[-1])):
#     print('----------------------------------------')
#     print('iteration' + str(i))
#     print(Obj[-1][i])
# #
# # -----------------------------------------------------------------------------
# # Visualization
# # -----------------------------------------------------------------------------
#
# SI, sg1, sg2 = Obj[-1][0][0]
#
# sg1_instances = sg1.subgroupDescription.covers(data)
# sg1_nodes = np.where(sg1_instances)[0]
#
# sg2_instances = sg2.subgroupDescription.covers(data)
# sg2_nodes = np.where(sg2_instances)[0]
#
# (sg_K, sg_N, sg_P) = task.qf.computeStatistics_BiCases(task.data, sg1, sg2)
#
# title = 'SI = ' + '{:.03f}'.format(SI) + '      D1: ' + str(sg1.subgroupDescription) + \
# '      D2: ' + str(sg2.subgroupDescription)+ '      Num: ' + str(len(sg_nodes))\
# + '      sg_K: ' + str(sg_K) +  '      sg_P = ' + '{:.03f}'.format(sg_P)
#
# indicate_bicluster_in_realNetwork(G, list(sg1_nodes), list(sg2_nodes), 'results/bi_lastfm/00.pdf', title)
