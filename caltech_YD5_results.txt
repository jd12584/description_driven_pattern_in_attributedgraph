Which dataset? 
 The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) Citeseer(Type c), Caltech36(Type ct), Reed98(Type r)-------------------------------------------------------------------
iteration0
(58.30565724729549, <<T: Density of connectivity; D: student/faculty status flag=1 AND year=2004>>, <<T: Density of connectivity; D: year=2008>>)
SI = 58.306      D1: student/faculty status flag=1 AND year=2004      D2: year=2008      Num1: 3      Num2: 173       Num_com: 0      sg_K: 108      sg_P = 25.232

(33.484390725742834, <<T: Density of connectivity; D: student/faculty status flag=1 AND year=2004>>, <<T: Density of connectivity; D: year=2008 AND second major=0>>)
SI = 33.484      D1: student/faculty status flag=1 AND year=2004      D2: year=2008 AND second major=0      Num1: 3      Num2: 114       Num_com: 0      sg_K: 71      sg_P = 15.671

(30.74836030732252, <<T: Density of connectivity; D: gender=2 AND year=2008>>, <<T: Density of connectivity; D: year=2004 AND student/faculty status flag=1>>)
SI = 30.748      D1: gender=2 AND year=2008      D2: year=2004 AND student/faculty status flag=1      Num1: 116      Num2: 3       Num_com: 0      sg_K: 71      sg_P = 16.966

(30.747849780722543, <<T: Density of connectivity; D: student/faculty status flag=1 AND year=2004>>, <<T: Density of connectivity; D: year=2008 AND gender=2>>)
SI = 30.748      D1: student/faculty status flag=1 AND year=2004      D2: year=2008 AND gender=2      Num1: 3      Num2: 116       Num_com: 0      sg_K: 71      sg_P = 16.966

(28.66564312113503, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19445>>)
SI = 28.666      D1: student/faculty status flag=1 AND dorm/house=166      D2: dorm/house=0 AND high school=19445      Num1: 53      Num2: 1       Num_com: 0      sg_K: 51      sg_P = 17.523

(28.66564312113503, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND high school=19445>>)
SI = 28.666      D1: student/faculty status flag=1 AND dorm/house=166      D2: student/faculty status flag=2 AND high school=19445      Num1: 53      Num2: 1       Num_com: 0      sg_K: 51      sg_P = 17.523

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: student/faculty status flag=2 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: dorm/house=0 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(25.43580985028782, <<T: Density of connectivity; D: student/faculty status flag=2 AND year=2004>>, <<T: Density of connectivity; D: year=2008>>)
SI = 25.436      D1: student/faculty status flag=2 AND year=2004      D2: year=2008      Num1: 32      Num2: 173       Num_com: 0      sg_K: 41      sg_P = 120.100

(25.087262457725192, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: student/faculty status flag=1 AND dorm/house=172      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

-------------------------------------------------------------------
iteration1
(28.66564312113503, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19445>>)
SI = 28.666      D1: student/faculty status flag=1 AND dorm/house=166      D2: dorm/house=0 AND high school=19445      Num1: 53      Num2: 1       Num_com: 0      sg_K: 51      sg_P = 17.523

(28.66564312113503, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND high school=19445>>)
SI = 28.666      D1: student/faculty status flag=1 AND dorm/house=166      D2: student/faculty status flag=2 AND high school=19445      Num1: 53      Num2: 1       Num_com: 0      sg_K: 51      sg_P = 17.523

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: student/faculty status flag=2 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: dorm/house=0 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(25.43580985028782, <<T: Density of connectivity; D: year=2004 AND student/faculty status flag=2>>, <<T: Density of connectivity; D: year=2008>>)
SI = 25.436      D1: year=2004 AND student/faculty status flag=2      D2: year=2008      Num1: 32      Num2: 173       Num_com: 0      sg_K: 41      sg_P = 120.100

(25.435139886527384, <<T: Density of connectivity; D: year=2008>>, <<T: Density of connectivity; D: year=2004 AND student/faculty status flag=2>>)
SI = 25.435      D1: year=2008      D2: year=2004 AND student/faculty status flag=2      Num1: 173      Num2: 32       Num_com: 0      sg_K: 41      sg_P = 120.099

(25.087262457725192, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

(24.146105063556792, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 24.146      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND high school=2747      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 5.837

(23.265916411389146, <<T: Density of connectivity; D: dorm/house=171>>, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>)
SI = 23.266      D1: dorm/house=171      D2: dorm/house=172 AND high school=1560      Num1: 67      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 7.873

(23.265785208092822, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>, <<T: Density of connectivity; D: dorm/house=171>>)
SI = 23.266      D1: dorm/house=172 AND high school=1560      D2: dorm/house=171      Num1: 1      Num2: 67       Num_com: 0      sg_K: 35      sg_P = 7.873

-------------------------------------------------------------------
iteration2
(36.68438747046591, <<T: Density of connectivity; D: dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19576>>)
SI = 36.684      D1: dorm/house=166      D2: dorm/house=0 AND high school=19576      Num1: 70      Num2: 1       Num_com: 0      sg_K: 37      sg_P = 5.048

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: dorm/house=0 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(27.01232878923206, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=166>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND high school=19576>>)
SI = 27.012      D1: student/faculty status flag=1 AND dorm/house=166      D2: student/faculty status flag=2 AND high school=19576      Num1: 53      Num2: 1       Num_com: 0      sg_K: 28      sg_P = 2.921

(25.43580985028782, <<T: Density of connectivity; D: student/faculty status flag=2 AND year=2004>>, <<T: Density of connectivity; D: year=2008>>)
SI = 25.436      D1: student/faculty status flag=2 AND year=2004      D2: year=2008      Num1: 32      Num2: 173       Num_com: 0      sg_K: 41      sg_P = 120.100

(25.435139886527384, <<T: Density of connectivity; D: year=2008>>, <<T: Density of connectivity; D: year=2004 AND student/faculty status flag=2>>)
SI = 25.435      D1: year=2008      D2: year=2004 AND student/faculty status flag=2      Num1: 173      Num2: 32       Num_com: 0      sg_K: 41      sg_P = 120.099

(25.087262457725192, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: student/faculty status flag=1 AND dorm/house=172      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

(24.146105063556792, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 24.146      D1: student/faculty status flag=1 AND dorm/house=172      D2: dorm/house=0 AND high school=2747      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 5.837

(23.265916411389146, <<T: Density of connectivity; D: dorm/house=171>>, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>)
SI = 23.266      D1: dorm/house=171      D2: dorm/house=172 AND high school=1560      Num1: 67      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 7.873

(21.865875676531264, <<T: Density of connectivity; D: student/faculty status flag=2 AND second major=199>>, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>)
SI = 21.866      D1: student/faculty status flag=2 AND second major=199      D2: student/faculty status flag=1 AND dorm/house=172      Num1: 2      Num2: 73       Num_com: 0      sg_K: 46      sg_P = 10.611

(21.472856094473794, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=169>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=50211>>)
SI = 21.473      D1: student/faculty status flag=1 AND dorm/house=169      D2: dorm/house=0 AND high school=50211      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 6.745

-------------------------------------------------------------------
iteration3
(25.43580985028782, <<T: Density of connectivity; D: student/faculty status flag=2 AND year=2004>>, <<T: Density of connectivity; D: year=2008>>)
SI = 25.436      D1: student/faculty status flag=2 AND year=2004      D2: year=2008      Num1: 32      Num2: 173       Num_com: 0      sg_K: 41      sg_P = 120.100

(25.435139886527384, <<T: Density of connectivity; D: year=2008>>, <<T: Density of connectivity; D: year=2004 AND student/faculty status flag=2>>)
SI = 25.435      D1: year=2008      D2: year=2004 AND student/faculty status flag=2      Num1: 173      Num2: 32       Num_com: 0      sg_K: 41      sg_P = 120.099

(25.087262457725192, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: student/faculty status flag=1 AND dorm/house=172      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

(24.146105063556792, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 24.146      D1: student/faculty status flag=1 AND dorm/house=172      D2: dorm/house=0 AND high school=2747      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 5.837

(23.265916411389146, <<T: Density of connectivity; D: dorm/house=171>>, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>)
SI = 23.266      D1: dorm/house=171      D2: dorm/house=172 AND high school=1560      Num1: 67      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 7.873

(21.865875676531264, <<T: Density of connectivity; D: student/faculty status flag=2 AND second major=199>>, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>)
SI = 21.866      D1: student/faculty status flag=2 AND second major=199      D2: student/faculty status flag=1 AND dorm/house=172      Num1: 2      Num2: 73       Num_com: 0      sg_K: 46      sg_P = 10.611

(21.472856094473794, <<T: Density of connectivity; D: dorm/house=169 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=50211>>)
SI = 21.473      D1: dorm/house=169 AND student/faculty status flag=1      D2: dorm/house=0 AND high school=50211      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 6.745

(21.37197026738459, <<T: Density of connectivity; D: dorm/house=168>>, <<T: Density of connectivity; D: dorm/house=165 AND high school=50666>>)
SI = 21.372      D1: dorm/house=168      D2: dorm/house=165 AND high school=50666      Num1: 76      Num2: 1       Num_com: 0      sg_K: 31      sg_P = 6.250

(21.370187448453688, <<T: Density of connectivity; D: dorm/house=165 AND high school=50666>>, <<T: Density of connectivity; D: dorm/house=168>>)
SI = 21.370      D1: dorm/house=165 AND high school=50666      D2: dorm/house=168      Num1: 1      Num2: 76       Num_com: 0      sg_K: 31      sg_P = 6.251

(21.165187645651724, <<T: Density of connectivity; D: student/faculty status flag=1 AND year=2004>>, <<T: Density of connectivity; D: student/faculty status flag=2>>)
SI = 21.165      D1: student/faculty status flag=1 AND year=2004      D2: student/faculty status flag=2      Num1: 3      Num2: 159       Num_com: 0      sg_K: 12      sg_P = 58.111

-------------------------------------------------------------------
iteration4
(30.34742359058617, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=23308>>)
SI = 30.347      D1: dorm/house=172      D2: dorm/house=0 AND high school=23308      Num1: 91      Num2: 1       Num_com: 0      sg_K: 50      sg_P = 12.557

(28.752635376358146, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 28.753      D1: dorm/house=172      D2: dorm/house=0 AND second major=199      Num1: 91      Num2: 1       Num_com: 0      sg_K: 50      sg_P = 13.222

(28.603632328600494, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 28.604      D1: dorm/house=172      D2: dorm/house=0 AND high school=2747      Num1: 91      Num2: 1       Num_com: 0      sg_K: 36      sg_P = 6.097

(25.087262457725192, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

(24.709581760508343, <<T: Density of connectivity; D: dorm/house=168>>, <<T: Density of connectivity; D: dorm/house=0 AND major=202>>)
SI = 24.710      D1: dorm/house=168      D2: dorm/house=0 AND major=202      Num1: 76      Num2: 6       Num_com: 0      sg_K: 78      sg_P = 27.157

(24.146105063556792, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 24.146      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND high school=2747      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 5.837

(23.29223323383248, <<T: Density of connectivity; D: dorm/house=171>>, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>)
SI = 23.292      D1: dorm/house=171      D2: dorm/house=172 AND high school=1560      Num1: 67      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 7.863

(23.29210094906303, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>, <<T: Density of connectivity; D: dorm/house=171>>)
SI = 23.292      D1: dorm/house=172 AND high school=1560      D2: dorm/house=171      Num1: 1      Num2: 67       Num_com: 0      sg_K: 35      sg_P = 7.863

(22.363811399895972, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=1942>>)
SI = 22.364      D1: dorm/house=172      D2: dorm/house=0 AND high school=1942      Num1: 91      Num2: 2       Num_com: 0      sg_K: 50      sg_P = 14.183

(21.871855085232706, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND second major=199>>)
SI = 21.872      D1: dorm/house=172 AND student/faculty status flag=1      D2: student/faculty status flag=2 AND second major=199      Num1: 73      Num2: 2       Num_com: 0      sg_K: 46      sg_P = 10.608

-------------------------------------------------------------------
iteration5
(28.752635376358146, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 28.753      D1: dorm/house=172      D2: dorm/house=0 AND second major=199      Num1: 91      Num2: 1       Num_com: 0      sg_K: 50      sg_P = 13.222

(28.603632328600494, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 28.604      D1: dorm/house=172      D2: dorm/house=0 AND high school=2747      Num1: 91      Num2: 1       Num_com: 0      sg_K: 36      sg_P = 6.097

(25.087262457725192, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND second major=199>>)
SI = 25.087      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND second major=199      Num1: 73      Num2: 1       Num_com: 0      sg_K: 45      sg_P = 10.593

(24.709581760508343, <<T: Density of connectivity; D: dorm/house=168>>, <<T: Density of connectivity; D: dorm/house=0 AND major=202>>)
SI = 24.710      D1: dorm/house=168      D2: dorm/house=0 AND major=202      Num1: 76      Num2: 6       Num_com: 0      sg_K: 78      sg_P = 27.157

(24.146105063556792, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=2747>>)
SI = 24.146      D1: dorm/house=172 AND student/faculty status flag=1      D2: dorm/house=0 AND high school=2747      Num1: 73      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 5.837

(23.29223323383248, <<T: Density of connectivity; D: dorm/house=171>>, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>)
SI = 23.292      D1: dorm/house=171      D2: dorm/house=172 AND high school=1560      Num1: 67      Num2: 1       Num_com: 0      sg_K: 35      sg_P = 7.863

(23.29210094906303, <<T: Density of connectivity; D: dorm/house=172 AND high school=1560>>, <<T: Density of connectivity; D: dorm/house=171>>)
SI = 23.292      D1: dorm/house=172 AND high school=1560      D2: dorm/house=171      Num1: 1      Num2: 67       Num_com: 0      sg_K: 35      sg_P = 7.863

(22.363811399895972, <<T: Density of connectivity; D: dorm/house=172>>, <<T: Density of connectivity; D: dorm/house=0 AND high school=1942>>)
SI = 22.364      D1: dorm/house=172      D2: dorm/house=0 AND high school=1942      Num1: 91      Num2: 2       Num_com: 0      sg_K: 50      sg_P = 14.183

(21.871855085232706, <<T: Density of connectivity; D: dorm/house=172 AND student/faculty status flag=1>>, <<T: Density of connectivity; D: student/faculty status flag=2 AND second major=199>>)
SI = 21.872      D1: dorm/house=172 AND student/faculty status flag=1      D2: student/faculty status flag=2 AND second major=199      Num1: 73      Num2: 2       Num_com: 0      sg_K: 46      sg_P = 10.608

(21.871625110204217, <<T: Density of connectivity; D: student/faculty status flag=2 AND second major=199>>, <<T: Density of connectivity; D: student/faculty status flag=1 AND dorm/house=172>>)
SI = 21.872      D1: student/faculty status flag=2 AND second major=199      D2: student/faculty status flag=1 AND dorm/house=172      Num1: 2      Num2: 73       Num_com: 0      sg_K: 46      sg_P = 10.608

