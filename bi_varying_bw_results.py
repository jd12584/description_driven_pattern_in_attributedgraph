import os
import pickle

import numpy as np
import matplotlib.pyplot as plt

cwd = os.getcwd()
file = os.path.join(cwd, *['bw_results', 'bi_lastfm_nocons_varyingbw.pkl'])
# file = os.path.join(cwd, *['bw_results', 'bi_Reed98_nocons_varyingbw.pkl'])

f = open(file,'rb')
Obj = pickle.load(f)
f.close()
print(Obj)

bw, time, results = Obj
n = len(bw)

SI_mat = np.ones((n,n))
time = np.array(time)

i = 0
for i in range(n):
    for j in range(n):
        print('------------------------------------------------\n')
        print('when x1 = '+ str(bw[i]) + '  x2 = ' + str(bw[j]) + '\n')
        print(results[i][j])
        SI_mat[i][j] = results[i][j][0][0]

print(bw)
print(time)

fig, axs = plt.subplots(1,3,figsize=(15,5))

# vmax = 800 if Reed98
im0 = axs[0].imshow(SI_mat[::-1], cmap = 'OrRd', vmin=140, vmax=240)

# cbar0 = plt.colorbar(im0, ax=axs[0], fraction=0.06, pad=0.08,orientation="horizontal")
# cbar0.ax.set_xlabel('The SI of the best pattern', fontsize=13, va='bottom', labelpad=12)
axs[0].set_xticks(np.arange(n))
axs[0].set_yticks(np.arange(n))
axs[0].set_xticklabels(bw, fontsize = 13.4)
axs[0].set_yticklabels(bw[::-1], fontsize = 13.4)
axs[0].set_xlabel(r'Inner beam width $x_2$', fontsize = 15)
axs[0].set_ylabel(r'Outer beam width $x_1$', fontsize = 15)
axs[0].set_title(r'(a) SI as a function of $x_1$ and $x_2$',fontsize = 16)
axs[0].tick_params(right=False, left=True,
               labelright=False, labelleft=True)
for i in range(n):
    for j in range(n):
        text = axs[0].text(j,i,round(SI_mat[::-1][i,j],1),ha="center", va="center", color='black',fontsize=12.9)


# im1 = axs[1].imshow(time, cmap = 'Purples')
# for i in range(n):
#     for j in range(n):
#         text = axs[1].text(j,i,round(time[i][j],2),ha="center", va="center")
# cbar1 = plt.colorbar(im1, ax=axs[1], fraction=0.06, pad=0.08,orientation="horizontal")
# cbar1.ax.set_xlabel('Run time (s)', fontsize=13, va='bottom', labelpad=12)
colors = ['orange','firebrick', 'mediumseagreen', 'dodgerblue', 'mediumpurple']
markers = ['o','s','*','p','d']
for i in range(n):
     axs[1].plot(bw, time[i,:],linestyle='--',marker=markers[i],color=colors[i],label=r'$x_1=%d$'%(bw[i]))

axs[1].set_xticks(bw)
axs[1].tick_params(axis='both', labelsize=13.4)
axs[1].set_xlabel(r'Inner beam width $x_2$', fontsize = 15)
axs[1].set_ylabel('Run time (s)', fontsize = 15)
axs[1].set_title(r'(b) Run time vs. $x_2$',fontsize = 16)
axs[1].legend()
# axs[1].grid(linestyle='--')

for i in range(n):
     axs[2].plot(bw, time[:,i],linestyle='--',marker=markers[i],color=colors[i],label=r'$x_2=%d$'%(bw[i]))

axs[2].set_xticks(bw)
axs[2].tick_params(axis='both', labelsize=13.4)
axs[2].set_xlabel(r'Outer beam width $x_1$', fontsize = 15)
axs[2].set_ylabel('Run time (s)', fontsize = 15)
axs[2].set_title(r'(c) Run time vs. $x_1$',fontsize = 16)
axs[2].legend()
# axs[2].grid(linestyle='--')

fig.tight_layout(pad=3.0)
plt.show()
