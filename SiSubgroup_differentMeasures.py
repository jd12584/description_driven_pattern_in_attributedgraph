import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import os

import time
import pickle

from visualisation import *
from get_graph_and_attributes import *



# -----------------------------------------------------------------------------
# Get a graph and the user-attributes data frame from a particular dataset
# -----------------------------------------------------------------------------

dataset = input('Which dataset? \n \
The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) \
Citeseer(Type c), Caltech36(Type ct), Reed98(Type r)')

if dataset == "s":
    G, data, name = from_old_synthesized_data()
    ud=False
elif dataset == "d":
    G, data, name = from_delicious(100)
    ud=True
elif dataset == "l":
    G, data, name = from_lastfm(11900)
    ud=True
elif dataset == "c":
    G, data, name = from_citeseer()
    ud=False
elif dataset == "ct":
    G, data, name = from_facebook100('Caltech36.mat')
    ud=True
elif dataset == "r":
    G, data, name = from_facebook100('Reed98.mat')
    ud=True
else:
    print("Invalid option.")

# adjacency matrix
total_A = nx.adjacency_matrix(G)


# -----------------------------------------------------------------------------
# Choosing Measurement
# -----------------------------------------------------------------------------

choice = input('IntraCluserDensity (Type 1) \n \
or Average Degree (Type 2) \n \
or InverseConductance (Type 3)\n \
or SimonMeasure (Type 4)\n \
or SegregationIndex (Type 5)?')

# Initial background distribution
#
bd_graph = BGDistr(total_A, datasource='custom')
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(iterations=1000,verbose=True,undirected=ud)

#
#
lambda_dict = {}
Result = []

target = psx.GTarget(G, x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict, ud)

searchspace = psx.createSelectors(data)
print(searchspace)
print(len(searchspace))

if choice == '1':
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[20],depth=2,qf=psx.graph_target.IntraClusterDensity())
    measure = 'EdgeDensity'

elif choice == '2':
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[20],depth=2,qf=psx.graph_target.AverageDegree())
    measure = 'AverageDegree'

elif choice == '3':
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[20],depth=2,qf=psx.graph_target.InverseConductance())
    measure = 'InvConuctance'

elif choice == '4':
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[20],depth=2,qf=psx.graph_target.SimonMeasure())
    measure = 'SimonM'

elif choice == '5':
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[20],depth=2,qf=psx.graph_target.SegregationIndex())
    measure = 'SIDX'

else:
    print("Invalid option.")

search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=30).execute(task)
search_time.append(time.time() - search_time_s)


Result.append(result)
print(result)

# save key variables
cwd = os.getcwd()
file = os.path.join(cwd, *['results','si_' + name + measure +'_results.pkl'])

Obj = (search_time,Result)
f = open(file,'wb')
pickle.dump(Obj, f)
f.close()



#
# # # -----------------------------------------------------------------------------
# # # Visualization
# # # -----------------------------------------------------------------------------
# #
# score, sg = Obj[-1][0][0]
#
# sg_instances = sg.subgroupDescription.covers(data)
# sg_nodes = np.where(sg_instances)[0]
#
# (sg_K, sg_N, sg_P) = sg.get_base_statistics(data, weightingAttribute=None)
#
# title = 'Score = ' + '{:.03f}'.format(score) + '      D: ' + str(sg.subgroupDescription)

# cwd = os.getcwd()
# file = os.path.join(cwd, *['results','si_' + name + measure,'.pdf'])


# indicate_cluster_in_realNetwork_u(G, list(sg_nodes), 'results/lastfm1.pdf', title)
