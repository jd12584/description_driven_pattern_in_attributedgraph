import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

import time
import pickle

import os

from visualisation import *


cwd = os.getcwd()
file_re = os.path.join(cwd, *['datasets','citeseer', 'citeseer.cites'])
file_attr = os.path.join(cwd, *['datasets','citeseer', 'citeseer.content'])


# -----------------------------------------------------------------------------
# Read the paper content data
# -----------------------------------------------------------------------------

data_attr = pd.read_csv(file_attr,
                    dtype=None,
                    header=None,
                    sep='\t')

print(data_attr.head())

data_attr[0] = data_attr[0].astype(str)
paper_ids = list(data_attr[0].values)
print(paper_ids[:10])

# -----------------------------------------------------------------------------
# Construct a grpah correspoinding to the friend relationship dataset
# -----------------------------------------------------------------------------

# Note in this data, if a line is represented by "paper1 paper2" then the link is "paper2->paper1"
data_re = np.genfromtxt(file_re,
                     # names=True,
                     dtype=None,
                     encoding="latin1",
                     usecols=(1,0))

print(data_re[1])

G=nx.DiGraph()

G.add_nodes_from(paper_ids)
G.add_edges_from(data_re)

N = len(G)
print(N)

mapping=dict(zip(G.nodes(),range(N)))
nx.relabel_nodes(G,mapping,copy=False)


total_A = nx.adjacency_matrix(G)


# -----------------------------------------------------------------------------
# Construct the user-attributes data frame
# -----------------------------------------------------------------------------

data_attr = pd.read_csv(file_attr,
                    dtype=None,
                    header=None,
                    sep='\t')

# print(data_attr.head())

data_attr[0] = data_attr[0].astype(str)

paperids = []
for i in list(data_attr[0].values):
    paperids.append(mapping[i])

print(paperids[:10])

data = data_attr.select_dtypes(include=['number'])
data.index = paperids



print(data.head())
# data.index = data.index.astype(str)
# data.columns = data.columns.astype(str)


# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')

bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(\
iterations=10000,verbose=True,undirected=False)
bg_time =  time.time() - bg_time_s

lambda_dict = {}

interesting_patterns = []
Result = []

# # -----------------------------------------------------------------------------
# # The first Search
# # -----------------------------------------------------------------------------
# target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
#
# searchspace = psx.createSelectors(data)
#
# task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=10,depth=3,qf=psx.SubjectiveInterestingness())
#
# search_time = []
# search_time_s = time.time()
# result = psx.BeamSearch(beamWidth=20).execute(task)
# search_time.append(time.time() - search_time_s)
#
# Result.append(result)

#
# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict, bg_time, search_time, Result)
# f = open('results/citeseer_results.pkl','wb')
# pickle.dump(Obj, f)
# f.close()

# f = open('results/citeseer_results.pkl','rb')
# Obj = pickle.load(f)
# f.close()
#
# print(Obj[-3])
# print(Obj[-2])
# print(Obj[-1])
#
#
# # -----------------------------------------------------------------------------
# # Visualization
# # -----------------------------------------------------------------------------
#
# SI, sg = Obj[-1][0][0]
#
# sg_instances = sg.subgroupDescription.covers(data)
# sg_nodes = np.where(sg_instances)[0]
# (sg_K, sg_N, sg_P) = sg.get_base_statistics(data, weightingAttribute=None)
#
#
# title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription) + '      Num: ' + str(len(sg_nodes))\
# + '      sg_K: ' + str(sg_K) +  '      sg_P: ' + str(sg_P)
#
# indicate_cluster_in_realNetwork_d(G, list(sg_nodes), 'results/citeseer.pdf', title)
