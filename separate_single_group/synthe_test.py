import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

import maxent

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *


from visualisation import *

import pickle
import time


# -----------------------------------------------------------------------------
# Synthesize the data set
# -----------------------------------------------------------------------------
# G = graphdata()

indptr=np.array([0,4,7,9,11,12,13,16,18,19,19])
indices=np.array([1,2,3,6,2,3,4,3,5,4,5,5,8,7,8,9,8,9,9])
data=np.ones(19)

A = csr_matrix((data, indices, indptr), dtype=np.int8,shape=(10,10))
print(A)

G = nx.from_scipy_sparse_matrix(A)
print(nx.adjacency_matrix(G))
# G.remove_edges_from(G.selfloop_edges())


# adding attributes
d = {'a': [1,0,1,0,1,0,0,1,0,0], 'b': [0,0,1,0,0,1,1,1,1,1], 'c': [1,1,0,1,0,1,0,0,0,0], \
'd': [0,0,0,0,1,0,0,0,1,0], 'e': [0,1,0,0,0,0,1,1,1,1]}
data = pd.DataFrame(data=d)
# print(data)



# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------


bd_graph = BGDistr(A.todense(), datasource='custom')


bg_time_s = time.time()

x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(verbose=True)

bg_time_e = time.time()
bg_time = bg_time_e - bg_time_s


lambda_dict = {}
Result = []



# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------

target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# print(nx.adjacency_matrix(target.graph))

searchspace = psx.createSelectors(data)
print(searchspace)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=2,qf=psx.SubjectiveInterestingness())


search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute(task)
search_time_e = time.time()

search_time.append(search_time_e - search_time_s)

Result.append(result)

total_A = nx.adjacency_matrix(G)



def Connecting_Prob(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing the current connecting probability of edges within the subgroup'''
    sg_P = 0

    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)


    for i in lambda_dict.keys():

        com_members_ids = []

        com_members = [j for j in lambda_dict[i] if j in sg_nodes]
        num_com_members = len(com_members)

        if com_members != []:

            com_members_ids = [sg_nodes.index(j) for j in com_members]

            row = np.asarray(com_members_ids*num_com_members)
            col = np.repeat(com_members_ids, num_com_members)

            value = [i]*(num_com_members**2)

            lambda_M = csr_matrix((value,(row,col)), shape=(sg_n, sg_n)).toarray()

            odds_M = P_M * np.exp(lambda_M)

            P_M = odds_M / (1. - P_M + odds_M)
        else:

            P_M = P_M

    return P_M


def f_constraint(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K


# -----------------------------------------------------------------------------
# The sequel
# -----------------------------------------------------------------------------

num_it = 5;
end_base = 50.

for it in range(num_it):

    _, sg = result[0]

    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)

    P_M = Connecting_Prob(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)

    sg_A = nx.adjacency_matrix(G,sg_nodes)
    sg_K = sg_A.count_nonzero()

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg_n, sg_K))
        end_base += 20.
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg_n, sg_K))

    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=2,qf=psx.SubjectiveInterestingness())

    result = psx.BeamSearch(beamWidth=10).execute(task)
    Result.append(result)

    print(lambda_dict)


for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])
# #

# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
# f = open('results/old_syn_results.pkl','wb')
# pickle.dump(Obj, f)
# f.close()


# f = open('results/old_syn_results.pkl','rb')
# Obj = pickle.load(f)
# f.close()
#
# print(Obj[-3])
# print(Obj[-2])
# print(Obj[-1])

# for i in range(len(Obj[-1])):
#     print('----------------------------------------')
#     print('iteration' + str(i))
#     print(Obj[-1][i])
#
#
# # -----------------------------------------------------------------------------
# # Visualization
# # -----------------------------------------------------------------------------
#
# SI, sg = Obj[-1][2][0]
#
# sg_instances = sg.subgroupDescription.covers(data)
# sg_nodes = np.where(sg_instances)[0]
# (sg_K, sg_N, sg_P) = sg.calculateStatistics(data, weightingAttribute=None)
#
# title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription)
#
# indicate_cluster_in_smallNetwork(G, list(sg_nodes), 'results/old_syn/old_syn20.pdf', title)
#
# plt.show()
