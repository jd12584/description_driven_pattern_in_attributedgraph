import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

import maxent

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from visualisation import *

import pickle
import os

import time



cwd = os.getcwd()
file = os.path.join(cwd, *['datasets','germany', 'socio_economics_germany_2009_v3.csv'])

total_data = pd.read_csv(file,
                sep=';',
                encoding="latin1")


# -----------------------------------------------------------------------------
# Construct a graph correspoinding to the friend relationship dataset
# -----------------------------------------------------------------------------

# the nodes
AreaCode = list(total_data['Area Code'])
AreaName = list(total_data['Area Name'])

N = len(AreaCode)
AreaMapping = dict(zip(AreaCode, AreaName))

# the edges
edgelist_dict = {k : [] for k in AreaCode}

# print(total_data.at[411,'Neighborhood'])

for i in range(N):
    neighbors = total_data.at[i ,'Neighborhood']

    for j in neighbors.split(','):

        edgelist_dict[AreaCode[i]].append(int(j))   # convert the string element to int


print(edgelist_dict[1001])
# the graph
G=nx.from_dict_of_lists(edgelist_dict)

# relabel the nodes by ordered numbers
node_mapping = dict(zip(G.nodes(),range(N)))


nx.relabel_nodes(G,node_mapping,copy=False)

# for i in range(N):
#     print(AreaName[node_mapping[AreaCode[0]]]==AreaMapping[AreaCode[0]])


print(node_mapping[1051])

total_A = nx.adjacency_matrix(G)
M = total_A.count_nonzero()
print(M)
# print(total_A.todense())



# -----------------------------------------------------------------------------
# Construct the user-attributes data frame
# -----------------------------------------------------------------------------
data = total_data.iloc[:,7:45]
data =data.replace(',','.', regex=True).astype(np.float16)
data.index = range(N)

print(data.head())

# for large cities
# data = total_data.iloc[:,22:23]
# data =data.replace(',','.', regex=True).astype(np.float16)
# data.index = range(N)
#
# typedata = list(total_data['Type'])

print(data.head())

# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')

bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(reltol=.01,iterations=2000,verbose=False)

bg_time =  time.time() - bg_time_s


lambda_dict = {}
Result = []
#
#
# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------
target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

searchspace = psx.createSelectors(data)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=3,qf=psx.SubjectiveInterestingness())

search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute(task)
search_time.append(time.time() - search_time_s)


Result.append(result)


total_A = nx.adjacency_matrix(G)


def Connecting_Prob(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing current connecting probability of edges within the subgroup'''
    sg_P = 0

    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)


    for i in lambda_dict.keys():

        com_members_ids = []

        com_members = [j for j in lambda_dict[i] if j in sg_nodes]
        num_com_members = len(com_members)

        if com_members != []:

            com_members_ids = [sg_nodes.index(j) for j in com_members]

            row = np.asarray(com_members_ids*num_com_members)
            col = np.repeat(com_members_ids, num_com_members)

            value = [i]*(num_com_members**2)

            lambda_M = csr_matrix((value,(row,col)), shape=(sg_n, sg_n)).toarray()

            odds_M = P_M * np.exp(lambda_M)

            P_M = odds_M / (1. - P_M + odds_M)
        else:

            P_M = P_M

    return P_M


def f_constraint(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K


# -----------------------------------------------------------------------------
# The sequel
# -----------------------------------------------------------------------------

num_it = 2;
end_base = 50.

for it in range(num_it):

    _, sg = result[0]

    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)

    P_M = Connecting_Prob(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)

    sg_A = nx.adjacency_matrix(G,sg_nodes)
    sg_K = sg_A.count_nonzero()

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg_n, sg_K))
        end_base += 20.
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg_n, sg_K))

    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=3,qf=psx.SubjectiveInterestingness())

    result = psx.BeamSearch(beamWidth=10).execute(task)
    Result.append(result)

    print(lambda_dict)


for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])
# #
# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
# f = open('results/germany_results.pkl','wb')
# pickle.dump(Obj, f)
# f.close()

# f = open('results/germany_results.pkl','rb')
# Obj = pickle.load(f)
# f.close()
#

# for i in range(len(Obj[-1])):
#     print(Obj[-1][i])
#     print('------------------------------------------------------------')



# #  -----------------------------------------------------------------------------
# #  Large cities
# #  -----------------------------------------------------------------------------
#
#
# target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# searchspace = psx.createSelectors(data)
# task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=10
# ,depth=1,qf=psx.SubjectiveInterestingness())
#
# result = psx.BeamSearch(beamWidth=10).execute(task)
# print(result)
# print(result[4])
#
#
#
# # -----------------------------------------------------------------------------
# # Visualization
# # -----------------------------------------------------------------------------
#
# # the latitudes and longitudes
#
# lat = []
# lon = []
#
# for i in range(N):
#      coordinates = total_data.at[i ,'Coordinates']
#
#      lat.append(np.float64(coordinates.split(',')[0]))
#      lon.append(np.float64(coordinates.split(',')[-1]))
#
# SI, sg = result[4]
# # SI, sg = Obj[-1][4][1]
#
# sg_instances = sg.subgroupDescription.covers(data)
# sg_nodes = np.where(sg_instances)[0]
#
# l = []
# for i in sg_nodes:
#     l.append((AreaName[i],typedata[i]))
#
# print(sorted(l))
#
# (sg_K, sg_N, sg_P) = sg.get_base_statistics(data, weightingAttribute=None)
#
# # title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription) + '      Num: ' + str(len(sg_nodes))\
# # + '      sg_K: ' + str(sg_K) +  '      sg_P = ' + '{:.03f}'.format(sg_P)
#
# title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription) + '      Num: ' + str(len(sg_nodes))\
# + '      sg_K: ' + str(sg_K)
#
# indicate_cluster_in_map(G, list(sg_nodes), lat, lon,'results/germany_urban.pdf', title)
#
# plt.show()
# #



#
#
#
