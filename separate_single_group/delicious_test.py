import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import pickle

import time
import os

from visualisation import *


cwd = os.getcwd()
file_re = os.path.join(cwd, *['datasets','Delicious', 'user_contacts.dat'])
file_tags = os.path.join(cwd, *['datasets','Delicious', 'tags.dat'])

file_attr = os.path.join(cwd, *['datasets','Delicious', 'user_taggedbookmarks.dat'])

# -----------------------------------------------------------------------------
# Read the tag dataset
# -----------------------------------------------------------------------------

data_tag = pd.read_csv(file_tags,
                    sep='\t',
                    encoding="latin1")


tagID = list(data_tag[data_tag.columns[0]])
tagValue = list(data_tag[data_tag.columns[-1]])

print(tagID[0])

tag_mapping = dict(zip(tagID,tagValue))


# -----------------------------------------------------------------------------
# Get all the users
# -----------------------------------------------------------------------------
data_user = pd.read_csv(file_attr,
                sep='\t',
                header=1,
                usecols=[0])

userID = data_user.values
print(userID[0])


# -----------------------------------------------------------------------------
# Construct a grpah correspoinding to the friend relationship dataset
# -----------------------------------------------------------------------------


data_re = np.genfromtxt(file_re,
                     names=True,
                     dtype=None,
                     usecols=(0,1))

print(data_re[0])

G=nx.Graph()

G.add_nodes_from(np.unique(userID))
G.add_edges_from(data_re)

N = len(G)
print(N)

node_mapping = dict(zip(G.nodes(),range(N)))
nx.relabel_nodes(G,node_mapping,copy=False)

print(node_mapping[8])

total_A = nx.adjacency_matrix(G)


#
# -----------------------------------------------------------------------------
# Construct the user-attributes data frame
# -----------------------------------------------------------------------------
#
data_attr = np.genfromtxt(file_attr,
                     names=True,
                     dtype=None,
                     usecols=(0, 2))


# a dicitonary with the keys as the users and values as the tags each user assigned

attr_dict = {node_mapping[k[0]]: set() for k in data_attr}

# a dictionary with the keys as the tags and values as the assignment frequency
# of each tag
attr_freq_dict = dict.fromkeys(tagValue,0)


##
## get the 1350 most frequent tagsprior
##


for k in data_attr:
    attr_dict[node_mapping[k[0]]].add(tag_mapping[k[-1]])
    attr_freq_dict[tag_mapping[k[-1]]] += 1

attrs = nlargest(1350, attr_freq_dict, key=attr_freq_dict.__getitem__)

attrs_set = set(attrs)

for k in range(N):
    attr_dict[k]= attr_dict[k].intersection(attrs_set)

##

mlb = MultiLabelBinarizer(classes=attrs)
attr_rec = mlb.fit_transform(list(attr_dict.values()))
# print(list(mlb.classes_))

for k in range(N):
    attr_dict[k] = list(attr_rec[k,:])

data = pd.DataFrame.from_dict(attr_dict, orient='index',columns=attrs)

print(data.head())
print(data.columns)

# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')

bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(reltol=.01,iterations=5000,verbose=False)

bg_time =  time.time() - bg_time_s

# print(x_rows)
# bg_dist = BackgroundDistribution(total_A, 'degree')
# bg_dist.fit()
# las = bg_dist.get_las()
# E = np.exp(las/2 + las.T/2)
# P = E/(1+E)
lambda_dict = {}
# print(np.sum(abs(total_A.sum(axis=1) - P.sum(axis=1))))

Result = []
#
#
# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------
target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# print(nx.adjacency_matrix(target.graph))

searchspace = psx.createSelectors(data)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=3,qf=psx.SubjectiveInterestingness())
#
search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute(task)
search_time.append(time.time() - search_time_s)

Result.append(result)
print(result)


total_A = nx.adjacency_matrix(G)



def Connecting_Prob(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing the current connecting probability of edges within the subgroup'''
    sg_P = 0

    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)


    for i in lambda_dict.keys():

        com_members_ids = []

        com_members = [j for j in lambda_dict[i] if j in sg_nodes]
        num_com_members = len(com_members)

        if com_members != []:

            com_members_ids = [sg_nodes.index(j) for j in com_members]

            row = np.asarray(com_members_ids*num_com_members)
            col = np.repeat(com_members_ids, num_com_members)

            value = [i]*(num_com_members**2)

            lambda_M = csr_matrix((value,(row,col)), shape=(sg_n, sg_n)).toarray()

            odds_M = P_M * np.exp(lambda_M)

            P_M = odds_M / (1. - P_M + odds_M)
        else:

            P_M = P_M

    return P_M



def f_constraint(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K


# -----------------------------------------------------------------------------
# The sequel
# -----------------------------------------------------------------------------

num_it = 2;
end_base = 50.

for it in range(num_it):

    _, sg = result[0]

    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)

    P_M = Connecting_Prob(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)

    sg_A = nx.adjacency_matrix(G,sg_nodes)
    sg_K = sg_A.count_nonzero()

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg_n, sg_K))
        end_base += 20.
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg_n, sg_K))

    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=3,qf=psx.SubjectiveInterestingness())

    result = psx.BeamSearch(beamWidth=10).execute(task)
    Result.append(result)

    print(lambda_dict)


for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])

#
# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
# f = open('results/delicious_results.pkl','wb')
# pickle.dump(Obj, f)
# f.close()

f = open('results/delicious_results.pkl','rb')
Obj = pickle.load(f)
f.close()

print(Obj[-3])
print(Obj[-2])
print(Obj[-1])


# -----------------------------------------------------------------------------
# Visualization
# -----------------------------------------------------------------------------
#
SI, sg = Obj[-1][0][0]

sg_instances = sg.subgroupDescription.covers(data)
sg_nodes = np.where(sg_instances)[0]

(sg_K, sg_N, sg_P) = sg.get_base_statistics(data, weightingAttribute=None)

title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription) + '      Num: ' + str(len(sg_nodes))\
+ '      sg_K: ' + str(sg_K) +  '      sg_P = ' + '{:.03f}'.format(sg_P)

indicate_cluster_in_realNetwork_u(G, list(sg_nodes), 'results/delicious1.pdf', title)

# # -----------------------------------------------------------------------------
# # SI
# # -----------------------------------------------------------------------------
# x_rows = Obj[0]
# x_columns = Obj[1]
# rowbeans_index = Obj[2]
# colbeans_index = Obj[3]
#
# lambda_dict = {}
# target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# # print(nx.adjacency_matrix(target.graph))
# #
# searchspace = psx.createSelectors(data)
# #
# task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=10,depth=3,qf=psx.SubjectiveInterestingness())
#
# quality = task.qf.evaluateFromDataset (task.data, sg)
# print('quality = %.4f' % quality)
#
# print(sg.subgroupDescription)
