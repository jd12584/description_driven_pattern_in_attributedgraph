import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import os

import time
import pickle

from visualisation import *


cwd = os.getcwd()
file_re = os.path.join(cwd, *['datasets','Lastfm', 'user_friends.dat'])
file_tags = os.path.join(cwd, *['datasets','Lastfm', 'tags.dat'])

file_attr = os.path.join(cwd, *['datasets','Lastfm', 'user_taggedartists.dat'])


# -----------------------------------------------------------------------------
# Read the tag dataset
# -----------------------------------------------------------------------------

data_tag = pd.read_csv(file_tags,
                    sep='\t',
                    encoding="latin1")


tagID = list(data_tag[data_tag.columns[0]])
tagValue = list(data_tag[data_tag.columns[-1]])

tag_mapping = dict(zip(tagID,tagValue))

print(tagID[0])


# -----------------------------------------------------------------------------
# Get all the users
# -----------------------------------------------------------------------------
data_user = pd.read_csv(file_attr,
                sep='\t',
                header=1,
                usecols=[0])

userID = data_user.values
print(userID[0])


# -----------------------------------------------------------------------------
# Construct a grpah correspoinding to the friend relationship dataset
# -----------------------------------------------------------------------------


data_re = np.genfromtxt(file_re,
                     names=True,
                     dtype=None,
                     usecols=(0,1))

print(data_re[0])

G=nx.Graph()

G.add_nodes_from(np.unique(userID))
G.add_edges_from(data_re)

N = len(G)
print(N)

node_mapping=dict(zip(G.nodes(),range(N)))
nx.relabel_nodes(G,node_mapping,copy=False)

print(node_mapping[8])

total_A = nx.adjacency_matrix(G)


#
# # -----------------------------------------------------------------------------
# # Construct the user-attributes data frame
# # -----------------------------------------------------------------------------
#
data_attr = np.genfromtxt(file_attr,
                     names=True,
                     dtype=None,
                     usecols=(0, 2))


# a dicitonary with the keys as the users and values as the tags each user assigned
attr_dict = {node_mapping[k[0]]: set() for k in data_attr}

# a dictionary with the keys as the tags and values as the assignment frequency
# of each tag
attr_freq_dict = dict.fromkeys(tagValue,0)


##
## get the 1350 most frequent tagsprior
##

for k in data_attr:
    attr_dict[node_mapping[k[0]]].add(tag_mapping[k[-1]])
    attr_freq_dict[tag_mapping[k[-1]]] += 1

attrs = nlargest(1350, attr_freq_dict, key=attr_freq_dict.__getitem__)

attrs_set = set(attrs)
for k in range(N):

    attr_dict[k]= attr_dict[k].intersection(attrs_set)


mlb = MultiLabelBinarizer(classes=attrs)
attr_rec = mlb.fit_transform(list(attr_dict.values()))
# print(list(mlb.classes_))

for k in range(N):
    attr_dict[k] = list(attr_rec[k,:])

data = pd.DataFrame.from_dict(attr_dict, orient='index',columns=attrs)

print(data.index)
# print(data.head())
print(data.columns)

# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')

bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(reltol=.01,iterations=5000,verbose=False)

bg_time =  time.time() - bg_time_s

lambda_dict = {}


Result = []
#
#
# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------
target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# print(nx.adjacency_matrix(target.graph))

searchspace = psx.createSelectors(data)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=3,qf=psx.SubjectiveInterestingness())


search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute(task)
search_time.append(time.time() - search_time_s)

Result.append(result)
#
#
# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
# f = open('results/lastfm_results.pkl','wb')
# pickle.dump(Obj, f)
# f.close()

f = open('results/lastfm_results.pkl','rb')
Obj = pickle.load(f)
f.close()

print(Obj[-3])
print(Obj[-2])
print(Obj[-1])

# -----------------------------------------------------------------------------
# Visualization
# -----------------------------------------------------------------------------

SI, sg = Obj[-1][0][0]

sg_instances = sg.subgroupDescription.covers(data)
sg_nodes = np.where(sg_instances)[0]
(sg_K, sg_N, sg_P) = sg.get_base_statistics(data, weightingAttribute=None)

title = 'SI = ' + '{:.03f}'.format(SI) + '      D: ' + str(sg.subgroupDescription) + '      Num: ' + str(len(sg_nodes))\
+ '      sg_K: ' + str(sg_K) +  '      sg_P = ' + '{:.03f}'.format(sg_P)

indicate_cluster_in_realNetwork_u(G, list(sg_nodes), 'results/lastfm1.pdf', title)
