import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx
import scipy

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize
from collections import defaultdict

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import pickle

import time
import os

from get_graph_and_attributes import *
from bg import *

def compute_store(data, searchspace):
    store = defaultdict(dict)
    for sel in searchspace:
        sg_instances = sel.covers(data)
        sg_nodes = list(np.where(sg_instances)[0])
        store[str(sel)] = sg_nodes
    return store

# -----------------------------------------------------------------------------
# Get a graph and the user-attributes data frame from a particular dataset
# -----------------------------------------------------------------------------

dataset = input('Which dataset? \n \
The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) \
Citeseer(Type c), Caltech36(Type ct), Reed98(Type r), DBLP topics as attributes(Type dbt)\
DBLP countries as attributes(Type dbc)')

if dataset == "s":
    G, data, name = from_old_synthesized_data()
    ud=False
elif dataset == "d":
    G, data, name = from_delicious(100)
    ud=True
elif dataset == "l":
    G, data, name = from_lastfm(10)
    ud=True
elif dataset == "c":
    G, data, name = from_citeseer()
    ud=False
elif dataset == "ct":
    G, data, name = from_facebook100('Caltech36.mat')
    ud=True
elif dataset == "r":
    G, data, name = from_facebook100('Reed98.mat')
    ud=True
elif dataset == "dbt":
    G, data, name = from_dblp_topics()
    ud=False
elif dataset == "dbc":
    G, data, name = from_dblp_affs()
    ud=False
else:
    print("Invalid option.")

# adjacency matrix
A = nx.adjacency_matrix(G)

# assert np.sum(np.abs(A - A.T)) != 0
# print('aaaaaaaaaaaaaaaaaaaaaaa', np.sum(np.abs(A - A.T)))
print(data.head())
print(data.columns)

# assert False


# -----------------------------------------------------------------------------
# Running choice
# -----------------------------------------------------------------------------
choice = input('Continue from a stored session (Type 1) \n \
or start a new search ? (Type 2)')

if choice == "1":
    cwd = os.getcwd()
    file = os.path.join(cwd, *['results','bi_' + name +'_time_results.pkl'])

    f = open(file,'rb')
    Obj = pickle.load(f)
    f.close()

    (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time, Result) = Obj
    result = Result[-1]
    print(result)

    initial_bg_paras = {}
    initial_bg_paras['x_rows'] = x_rows
    initial_bg_paras['x_columns'] = x_columns
    initial_bg_paras['rowbeans_index'] = rowbeans_index
    initial_bg_paras['colbeans_index'] = colbeans_index

elif choice == "2":
    #
    # Initial background distribution
    #
    which_prior = input('Pior beliefs on indiviual vertex degrees  (Type i) \n \
    or overall degree ? (Type o)')

    if which_prior == 'i':
        name = name + '_individual'
        initial_bg_paras, bg_time = initial_bg(A, ud, 'individual')
        check_convergence_indPrior(initial_bg_paras, A)

    elif which_prior == 'o':
        name = name +'_uniform'
        initial_bg_paras, bg_time = initial_bg(A, ud, 'uniform')

    else:
        print('Invalid option')
    lambda_dict = {}
    search_time = []
    Result = []

searchspace = psx.createSelectors(data)
print(searchspace)
print(len(searchspace))

store = compute_store(data, searchspace)
target = psx.ATarget(A, initial_bg_paras, lambda_dict, ud, store)

<<<<<<< HEAD
num_it = 2
=======
num_it = 5
>>>>>>> ca534eaf08c334bad5719f927c6edea878a213ce
for it in range(num_it):

    if it > 0:
        target.lambda_dict = update_bg_bi_sg(top_sgs[0], data, A, lambda_dict, ud)
    lambda_dict = target.lambda_dict

<<<<<<< HEAD
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[6,8],\
=======
    # task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[6,8],\
    # depth=2,qf=psx.adj_target.SubjectiveInterestingness())
    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[5,7],\
>>>>>>> ca534eaf08c334bad5719f927c6edea878a213ce
    depth=2,qf=psx.adj_target.SubjectiveInterestingness())

    search_time_s = time.time()
    top_sgs = psx.BeamSearch(beamWidth=30).execute_in_bi_sgs(task)
    # top_sgs = psx.BeamSearch(beamWidth=30).execute_in_bicases_constraints(task)
    search_time_e = time.time()
    search_time.append(search_time_e - search_time_s)
    print(top_sgs)
    Result.append(top_sgs)
<<<<<<< HEAD

    # # save key variables
    # cwd = os.getcwd()
    # file = os.path.join(cwd, *['results','bi_' + name + 'itr{:d}.pkl'.format(it)])
    # Obj = (initial_bg_paras['x_rows'], initial_bg_paras['x_columns'], initial_bg_paras['rowbeans_index'], \
    # initial_bg_paras[colbeans_index],lambda_dict,bg_time, search_time,Result)
    # f = open(file,'wb')
    # pickle.dump(Obj, f)
=======
    print(search_time)

    # save key variables
    cwd = os.getcwd()
    file = os.path.join(cwd, *['results','bi_' + name + 'itr{:d}.pkl'.format(it)])
    Obj = (initial_bg_paras['x_rows'], initial_bg_paras['x_columns'], initial_bg_paras['rowbeans_index'], \
    initial_bg_paras[colbeans_index],lambda_dict,bg_time, search_time,Result)
    f = open(file,'wb')
    pickle.dump(Obj, f)
>>>>>>> ca534eaf08c334bad5719f927c6edea878a213ce

print(search_time)
for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])
