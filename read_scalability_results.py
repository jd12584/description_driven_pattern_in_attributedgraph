import pickle
import os


cwd = os.getcwd()
# file = os.path.join(cwd, *['results','bi_Caltech36_individual_results.pkl'])
# file = os.path.join(cwd, *['results','bi_Reed98_individual_results.pkl'])
# file = os.path.join(cwd, *['results','bi_lastfm_individual_results.pkl'])
# file = os.path.join(cwd, *['results','bi_dblp_affs_uniform_results.pkl'])

# file = os.path.join(cwd, *['results','si_lastfm_individual_itr0.pkl'])
file = os.path.join(cwd, *['results','si_dblp_affs_individual_itr0.pkl'])

f = open(file,'rb')
Obj = pickle.load(f)
_, _, _, _, _, _, time, results =Obj
print(time)
print(results)
