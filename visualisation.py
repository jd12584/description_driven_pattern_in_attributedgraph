
import matplotlib.pyplot as plt
# from mpl_toolkits.basemap import Basemap

import networkx as nx


def indicate_cluster_in_smallNetwork (G, sg_nodes, filename, titlename):
    # nodes
    total_nodes = list(G.nodes())
    rest_nodes = [x for x in total_nodes if x not in sg_nodes]

    # pos = nx.random_layout(G)
    # pos = nx.spring_layout(G)
    pos = nx.fruchterman_reingold_layout(G)

    nx.draw_networkx(G, pos, \
            with_labels=True,
            node_size=500,
            width=1,
            node_color='darkgoldenrod')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg_nodes,
                        node_size=500,
                        node_color='orangered')


    # edges

    H = G.subgraph(sg_nodes)
    sg_edges = list(H.edges())
    nx.draw_networkx_edges(G, pos,\
                        edgelist=sg_edges,
                        width=8., alpha=.5,
                        edge_color='orangered')

    plt.axis('off')

    plt.title(titlename)

    plt.savefig(filename)
    plt.show()


def indicate_cluster_in_realNetwork_d (G, sg_nodes, filename, titlename):
    # nodes
    total_nodes = list(G.nodes())
    rest_nodes = [x for x in total_nodes if x not in sg_nodes]

    # pos = nx.random_layout(G)
    # pos = nx.fruchterman_reingold_layout(G)
    pos = nx.spring_layout(G)

    plt.figure(figsize=(18,10))

    nx.draw_networkx(G, pos, \
            with_labels=False,
            node_size=4,
            edge_size=.1,
            node_color='red',
            # edge_color='red',
            arrows=False,
            alpha=0.5)
            # alpha=.3)

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg_nodes,
                        node_size=6,
                        node_color='blue',
                        alpha=.8)

    # edges

    H = G.subgraph(sg_nodes)
    sg_edges = list(H.edges())

    nx.draw_networkx_edges(G, pos,\
                        edgelist=sg_edges,
                        width=.3, alpha=.8,
                        arrows=False,
                        edge_color='blue')

    plt.axis('off')

    plt.title(titlename)

    plt.savefig(filename)
    plt.show()


def indicate_cluster_in_realNetwork_u (G, sg_nodes, filename, titlename):
    # nodes
    total_nodes = list(G.nodes())
    rest_nodes = [x for x in total_nodes if x not in sg_nodes]

    # pos = nx.random_layout(G)
    # pos = nx.fruchterman_reingold_layout(G)
    pos = nx.spring_layout(G)
    # pos = nx.kamada_kawai_layout(G)
    # pos = nx.spectral_layout(G)

    plt.figure(figsize=(18,10))

    nx.draw_networkx(G, pos, \
            with_labels=False,
            node_size=4,
            edge_size=.1,
            node_color='red',
            alpha=0.2)

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg_nodes,
                        node_size=6,
                        node_color='blue',
                        alpha=.7)

    # edges

    H = G.subgraph(sg_nodes)
    sg_edges = list(H.edges())

    nx.draw_networkx_edges(G, pos,\
                        edgelist=sg_edges,
                        width=1.5, alpha=.6,
                        edge_color='blue')

    plt.axis('off')

    plt.title(titlename)

    plt.savefig(filename)
    plt.show()

# def indicate_cluster_in_map (G, sg_nodes, lat, lon, filename, titlename):
#     #determine range to print based on min, max lat and lon of the data
#     margin = 0.6 # buffer to add to the range
#     lat_min = min(lat) - margin
#     lat_max = max(lat) + margin
#     lon_min = min(lon) - margin
#     lon_max = max(lon) + margin
#
#     # create map using BASEMAP
#     m = Basemap(llcrnrlon=lon_min,
#             llcrnrlat=lat_min,
#             urcrnrlon=lon_max,
#             urcrnrlat=lat_max,
#             lat_0=(lat_max - lat_min)/2,
#             lon_0=(lon_max-lon_min)/2,
#             projection='merc',
#             resolution = 'h',
#             area_thresh=10000.,
#             )
#     m.drawcoastlines()
#     m.drawcountries(linewidth=0.6)
#     m.drawstates()
#     m.drawmapboundary(fill_color='lightblue')
#     # m.fillcontinents(color = 'papayawhip',lake_color='#46bcec')
#     m.fillcontinents(color = 'moccasin',lake_color='lightblue')
#
#     # convert lat and lon to map projection coordinates
#     lons, lats = m(lon, lat)
#
#     # coordinates = zip(lons,lats)
#
#     N = len(G)
#
#     pos = {i : [] for i in range(N)}
#
#     for i in range(N):
#         pos[i].append(lons[i])
#         pos[i].append(lats[i])
#         pos[i] = tuple(pos[i])
#
#     # nodes
#
#     total_nodes = list(G.nodes())
#     rest_nodes = [x for x in total_nodes if x not in sg_nodes]
#
#     nx.draw_networkx(G, pos, \
#             with_labels=False,
#             node_size=10,
#             width=.5,
#             node_color='red',
#             alpha = .3)
#
#     nx.draw_networkx_nodes(G, pos, \
#                         nodelist=sg_nodes,
#                         node_size=16,
#                         node_color='dodgerblue')
#
#
#     # edges
#
#     H = G.subgraph(sg_nodes)
#     sg_edges = list(H.edges())
#     nx.draw_networkx_edges(G, pos,\
#                         edgelist=sg_edges,
#                         width=2., alpha=.6,
#                         edge_color='dodgerblue')
#
#     plt.axis('off')
#
#     plt.title(titlename)
#
#     plt.savefig(filename)
#     plt.show()


def indicate_bicluster_in_smallNetwork (G, sg_nodes1, sg_nodes2, filename, titlename):
    # nodes
    total_nodes = list(G.nodes())
    com_nodes = [i for i in sg_nodes1 if i in sg_nodes2]

    # pos = nx.random_layout(G)
    # pos = nx.spring_layout(G)
    pos = nx.fruchterman_reingold_layout(G)

    nx.draw_networkx(G, pos, \
            with_labels=True,
            node_size=500,
            width=1,
            node_color='darkgoldenrod')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg_nodes1,
                        node_size=500,
                        node_color='orangered')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg_nodes2,
                        node_size=500,
                        node_color='dodgerblue')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=com_nodes,
                        node_size=500,
                        node_color='mediumorchid')

    # edges
    sg_edges = []
    edges = G.edges()
    for edge in edges:
        if edge[0] in sg_nodes1 and edge[1] in sg_nodes2:
            sg_edges.append(edge)

    nx.draw_networkx_edges(G, pos,\
                        edgelist=sg_edges,
                        width=6., alpha=.4,
                        edge_color='orangered')

    plt.axis('off')

    plt.title(titlename)

    # plt.savefig(filename)
    plt.show()


def indicate_bicluster_in_realNetwork (G, sg_nodes1, sg_nodes2, filename, titlename):
    # nodes
    total_nodes = list(G.nodes())
    com_nodes = list(set(sg_nodes1)&set(sg_nodes2))
    Adj_M = nx.adjacency_matrix(G)

    N = len(G)

    reverse_node_mapping=dict(zip(range(N), G.nodes()))
    sg1_mapping = [reverse_node_mapping[i] for i in sg_nodes1]
    sg2_mapping = [reverse_node_mapping[i] for i in sg_nodes2]
    com_nodes_mapping = [reverse_node_mapping[i] for i in com_nodes]

    # pos = nx.random_layout(G)
    # pos = nx.spring_layout(G)
    # pos = nx.kamada_kawai_layout(G)
    # pos = nx.spectral_layout(G)
    pos = nx.fruchterman_reingold_layout(G)
    plt.figure(figsize=(18,10))

    nx.draw_networkx(G, pos, \
            with_labels=False,
            node_size=2.5,
            width=.4,
            node_color='darkgoldenrod',
            alpha=.2)

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg1_mapping,
                        node_size=4,
                        node_color='orangered')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=sg2_mapping,
                        node_size=4,
                        node_color='dodgerblue')

    nx.draw_networkx_nodes(G, pos, \
                        nodelist=com_nodes_mapping,
                        node_size=4,
                        node_color='mediumorchid')

    # edges
    sg_edges = []
    edges = G.edges()
    num_com_edges = 0

    # Note these are problematic
    # for edge in edges:
    #     if (edge[0] in sg1_mapping and edge[1] in sg2_mapping) or (edge[0]\
    #      in sg2_mapping and edge[]):
    #         sg_edges.append(edge)
    #         if edge[0] in com_nodes_mapping and edge[1] in com_nodes_mapping:
    #             num_com_edges+=1

    for i in sg_nodes1:
        for j in sg_nodes2:
            if Adj_M[i,j] != 0:
                sg_edges.append((reverse_node_mapping[i], reverse_node_mapping[j]))
                if i in com_nodes and j in com_nodes:
                     num_com_edges+=1


    print(len(sg_edges))
    print(num_com_edges)

    nx.draw_networkx_edges(G, pos,\
                        edgelist=sg_edges,
                        width=1., alpha=.8,
                        edge_color='orangered')

    plt.axis('off')

    plt.title(titlename)

    plt.savefig(filename)
    plt.show()
