import matplotlib
import matplotlib.pyplot as plt
import numpy as np

font = {'family' : 'normal',
    'weight' : 'normal',
    'size'   : 20}

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **font)

x=[10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480]
y=[0.45, 1.53, 2.13, 3.90, 7.26, 14.27, 27.37, 51.94, 93.51, 180.83, 355.70, 697.57]

plt.figure(1,figsize=(8,3))

plt.plot(x, y, marker='o', linestyle='--', color='b', linewidth=2.5, markersize=12)
plt.xlim(8,21100)
plt.ylim(0.440,800)
# plt.xscale('log', basex=2)
# plt.yscale('log', basey=2)
plt.xticks(x, x)

for i,j in zip(x,y):
    plt.text(i-i*0.4,j+j*0.16,str(j),fontsize=25)

plt.xlabel(r'$|S|$',fontsize=25)
# plt.xlabel('|S|',fontsize=16)
plt.ylabel('Running time (s)', fontsize=25)

# plt.grid(True)
plt.show()
