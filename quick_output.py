import pickle
import os
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
cwd = os.getcwd()
file = os.path.join(cwd, *['results','si_dblp_affs_individual_itr0.pkl'])

f = open(file,'rb')
Obj = pickle.load(f)
x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result = Obj
f.close()
print(search_time)
