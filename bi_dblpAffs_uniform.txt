Which dataset? 
 The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) Citeseer(Type c), Caltech36(Type ct), Reed98(Type r), DBLP topics as attributes(Type dbt)DBLP countries as attributes(Type dbc)  (2, 1497)	1.0
  (18, 841)	1.0
  (19, 1189)	1.0
  (19, 3019)	1.0
  (24, 956)	1.0
  (24, 5033)	1.0
  (33, 3029)	1.0
  (36, 2616)	1.0
  (36, 2910)	1.0
  (38, 484)	1.0
  (39, 2684)	1.0
  (40, 4700)	1.0
  (40, 6401)	1.0
  (41, 192)	1.0
  (41, 2686)	1.0
  (42, 3097)	1.0
  (43, 2638)	1.0
  (43, 4062)	1.0
  (54, 74)	1.0
  (54, 729)	1.0
  (69, 2936)	1.0
  (69, 3088)	1.0
  (78, 3297)	1.0
  (81, 1843)	1.0
  (84, 734)	1.0
  :	:
  (6405, 1806)	1.0
  (6416, 1350)	1.0
  (6416, 3974)	1.0
  (6417, 1043)	1.0
  (6417, 3170)	1.0
  (6418, 644)	1.0
  (6418, 1098)	1.0
  (6427, 1499)	1.0
  (6427, 4322)	1.0
  (6437, 4469)	1.0
  (6438, 741)	1.0
  (6438, 2338)	1.0
  (6438, 2353)	1.0
  (6439, 3918)	1.0
  (6443, 2942)	1.0
  (6443, 4154)	1.0
  (6446, 6243)	1.0
  (6450, 1529)	1.0
  (6452, 1445)	1.0
  (6456, 3717)	1.0
  (6456, 6292)	1.0
  (6456, 6323)	1.0
  (6461, 367)	1.0
  (6461, 2838)	1.0
  (6467, 1339)	1.0
  (2, 1497)	1.0
  (18, 841)	1.0
  (19, 1189)	1.0
  (19, 3019)	1.0
  (24, 956)	1.0
  (24, 5033)	1.0
  (33, 3029)	1.0
  (36, 2616)	1.0
  (36, 2910)	1.0
  (38, 484)	1.0
  (39, 2684)	1.0
  (40, 4700)	1.0
  (40, 6401)	1.0
  (41, 192)	1.0
  (41, 2686)	1.0
  (42, 3097)	1.0
  (43, 2638)	1.0
  (43, 4062)	1.0
  (54, 74)	1.0
  (54, 729)	1.0
  (69, 2936)	1.0
  (69, 3088)	1.0
  (78, 3297)	1.0
  (81, 1843)	1.0
  (84, 734)	1.0
  :	:
  (6405, 1806)	1.0
  (6416, 1350)	1.0
  (6416, 3974)	1.0
  (6417, 1043)	1.0
  (6417, 3170)	1.0
  (6418, 644)	1.0
  (6418, 1098)	1.0
  (6427, 1499)	1.0
  (6427, 4322)	1.0
  (6437, 4469)	1.0
  (6438, 741)	1.0
  (6438, 2338)	1.0
  (6438, 2353)	1.0
  (6439, 3918)	1.0
  (6443, 2942)	1.0
  (6443, 4154)	1.0
  (6446, 6243)	1.0
  (6450, 1529)	1.0
  (6452, 1445)	1.0
  (6456, 3717)	1.0
  (6456, 6292)	1.0
  (6456, 6323)	1.0
  (6461, 367)	1.0
  (6461, 2838)	1.0
  (6467, 1339)	1.0
Pior beliefs on indiviual vertex degrees  (Type i) 
 or overall degree ? (Type o)-------------------------------------------------------------------
iteration0
(139.86275595404697, <<T: Density of connectivity; D: United States=1>>, <<T: Density of connectivity; D: United States=0>>)
SI = 139.863      D1: United States=1      D2: United States=0      Num1: 3132      Num2: 3340       Num_com: 0      sg_K: 335      sg_P = 765.827

(122.65001538379344, <<T: Density of connectivity; D: China=0 AND United States=1>>, <<T: Density of connectivity; D: United States=0>>)
SI = 122.650      D1: China=0 AND United States=1      D2: United States=0      Num1: 2969      Num2: 3340       Num_com: 0      sg_K: 288      sg_P = 725.970

(114.9465629000455, <<T: Density of connectivity; D: United States=1 AND Australia=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 114.947      D1: United States=1 AND Australia=0      D2: United States=0      Num1: 3092      Num2: 3340       Num_com: 0      sg_K: 320      sg_P = 756.046

(113.93168791400493, <<T: Density of connectivity; D: United States=1 AND Singapore=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 113.932      D1: United States=1 AND Singapore=0      D2: United States=0      Num1: 3088      Num2: 3340       Num_com: 0      sg_K: 321      sg_P = 755.068

(113.17254396188265, <<T: Density of connectivity; D: United States=1>>, <<T: Density of connectivity; D: United States=0 AND Canada=0>>)
SI = 113.173      D1: United States=1      D2: United States=0 AND Canada=0      Num1: 3132      Num2: 3023       Num_com: 0      sg_K: 281      sg_P = 693.142

(112.49800819079196, <<T: Density of connectivity; D: United States=1>>, <<T: Density of connectivity; D: United States=0 AND Singapore=0>>)
SI = 112.498      D1: United States=1      D2: United States=0 AND Singapore=0      Num1: 3132      Num2: 3128       Num_com: 0      sg_K: 298      sg_P = 717.217

(111.91531722769022, <<T: Density of connectivity; D: United States=1 AND AZ=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 111.915      D1: United States=1 AND AZ=0      D2: United States=0      Num1: 3080      Num2: 3340       Num_com: 0      sg_K: 323      sg_P = 753.112

(111.5858978926432, <<T: Density of connectivity; D: United States=1 AND Netherlands=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 111.586      D1: United States=1 AND Netherlands=0      D2: United States=0      Num1: 3113      Num2: 3340       Num_com: 0      sg_K: 329      sg_P = 761.181

(111.22997072257635, <<T: Density of connectivity; D: NJ=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 111.230      D1: NJ=0      D2: NJ=1 AND CA=1      Num1: 6292      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.909

(111.18573047633882, <<T: Density of connectivity; D: United States=1 AND Spain=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 111.186      D1: United States=1 AND Spain=0      D2: United States=0      Num1: 3115      Num2: 3340       Num_com: 0      sg_K: 330      sg_P = 761.670

(111.1494462936636, <<T: Density of connectivity; D: United States=1 AND MI=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 111.149      D1: United States=1 AND MI=0      D2: United States=0      Num1: 3042      Num2: 3340       Num_com: 0      sg_K: 318      sg_P = 743.820

(111.0132524039135, <<T: Density of connectivity; D: Germany=0 AND United States=1>>, <<T: Density of connectivity; D: United States=0>>)
SI = 111.013      D1: Germany=0 AND United States=1      D2: United States=0      Num1: 3077      Num2: 3340       Num_com: 0      sg_K: 324      sg_P = 752.378

(110.89601990312661, <<T: Density of connectivity; D: United States=1 AND Italy=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 110.896      D1: United States=1 AND Italy=0      D2: United States=0      Num1: 3100      Num2: 3340       Num_com: 0      sg_K: 328      sg_P = 758.002

(110.88286947720206, <<T: Density of connectivity; D: United States=1 AND Qatar=0>>, <<T: Density of connectivity; D: United States=0>>)
SI = 110.883      D1: United States=1 AND Qatar=0      D2: United States=0      Num1: 3124      Num2: 3340       Num_com: 0      sg_K: 332      sg_P = 763.870

-------------------------------------------------------------------
iteration1
(111.22997072257635, <<T: Density of connectivity; D: NJ=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 111.230      D1: NJ=0      D2: NJ=1 AND CA=1      Num1: 6292      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.909

(105.20125228509983, <<T: Density of connectivity; D: CA=0>>, <<T: Density of connectivity; D: CA=1 AND NJ=1>>)
SI = 105.201      D1: CA=0      D2: CA=1 AND NJ=1      Num1: 5584      Num2: 15       Num_com: 0      sg_K: 86      sg_P = 6.132

(92.73405673952348, <<T: Density of connectivity; D: NJ=0 AND Israel=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.734      D1: NJ=0 AND Israel=0      D2: NJ=1 AND CA=1      Num1: 6153      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.757

(92.66877968004344, <<T: Density of connectivity; D: NJ=0 AND MA=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.669      D1: NJ=0 AND MA=0      D2: NJ=1 AND CA=1      Num1: 5975      Num2: 15       Num_com: 0      sg_K: 92      sg_P = 6.561

(92.65163469554236, <<T: Density of connectivity; D: NJ=0 AND Switzerland=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.652      D1: NJ=0 AND Switzerland=0      D2: NJ=1 AND CA=1      Num1: 6163      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.768

(92.60225070268743, <<T: Density of connectivity; D: NJ=0 AND Spain=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.602      D1: NJ=0 AND Spain=0      D2: NJ=1 AND CA=1      Num1: 6169      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.774

(92.37247537903959, <<T: Density of connectivity; D: NJ=0 AND MD=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.372      D1: NJ=0 AND MD=0      D2: NJ=1 AND CA=1      Num1: 6197      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.805

(92.18456814304857, <<T: Density of connectivity; D: NJ=0 AND IN=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.185      D1: NJ=0 AND IN=0      D2: NJ=1 AND CA=1      Num1: 6220      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.830

(92.11938490018795, <<T: Density of connectivity; D: NJ=0 AND Greece=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.119      D1: NJ=0 AND Greece=0      D2: NJ=1 AND CA=1      Num1: 6228      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.839

(92.11938490018795, <<T: Density of connectivity; D: NJ=0 AND NC=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.119      D1: NJ=0 AND NC=0      D2: NJ=1 AND CA=1      Num1: 6228      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.839

(92.11124335264451, <<T: Density of connectivity; D: NJ=0 AND Austria=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.111      D1: NJ=0 AND Austria=0      D2: NJ=1 AND CA=1      Num1: 6229      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.840

(92.01365473669068, <<T: Density of connectivity; D: NJ=0 AND AZ=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 92.014      D1: NJ=0 AND AZ=0      D2: NJ=1 AND CA=1      Num1: 6241      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.853

(91.9487081803743, <<T: Density of connectivity; D: NJ=0 AND Ireland=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 91.949      D1: NJ=0 AND Ireland=0      D2: NJ=1 AND CA=1      Num1: 6249      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.862

(91.91626857205331, <<T: Density of connectivity; D: NJ=0 AND Sweden=0>>, <<T: Density of connectivity; D: NJ=1 AND CA=1>>)
SI = 91.916      D1: NJ=0 AND Sweden=0      D2: NJ=1 AND CA=1      Num1: 6253      Num2: 15       Num_com: 0      sg_K: 93      sg_P = 6.867

-------------------------------------------------------------------
iteration2
(32.69078875895389, <<T: Density of connectivity; D: China=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 32.691      D1: China=0      D2: China=1      Num1: 5599      Num2: 873       Num_com: 0      sg_K: 144      sg_P = 271.022

(31.73413986130527, <<T: Density of connectivity; D: China=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 31.734      D1: China=0      D2: China=1 AND IL=0      Num1: 5599      Num2: 861       Num_com: 0      sg_K: 128      sg_P = 266.103

(30.207567461279105, <<T: Density of connectivity; D: China=0 AND United States=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 30.208      D1: China=0 AND United States=0      D2: China=1      Num1: 2630      Num2: 873       Num_com: 0      sg_K: 64      sg_P = 168.086

(29.19759903536834, <<T: Density of connectivity; D: CA=1>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 29.198      D1: CA=1      D2: CA=0 AND WA=1      Num1: 888      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.962

(28.37577105788593, <<T: Density of connectivity; D: IL=0 AND China=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 28.376      D1: IL=0 AND China=0      D2: China=1      Num1: 5431      Num2: 873       Num_com: 0      sg_K: 134      sg_P = 265.197

(28.319504581450893, <<T: Density of connectivity; D: China=0 AND Spain=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 28.320      D1: China=0 AND Spain=0      D2: China=1      Num1: 5476      Num2: 873       Num_com: 0      sg_K: 133      sg_P = 263.658

(28.19180581196169, <<T: Density of connectivity; D: China=0 AND United States=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 28.192      D1: China=0 AND United States=0      D2: China=1 AND IL=0      Num1: 2630      Num2: 861       Num_com: 0      sg_K: 57      sg_P = 165.776

(28.07355960094299, <<T: Density of connectivity; D: China=0 AND NY=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 28.074      D1: China=0 AND NY=0      D2: China=1      Num1: 5247      Num2: 873       Num_com: 0      sg_K: 130      sg_P = 258.818

(27.984137963511927, <<T: Density of connectivity; D: China=0 AND Australia=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 27.984      D1: China=0 AND Australia=0      D2: China=1 AND IL=0      Num1: 5359      Num2: 861       Num_com: 0      sg_K: 114      sg_P = 251.999

(27.786727420553696, <<T: Density of connectivity; D: Singapore=0 AND China=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 27.787      D1: Singapore=0 AND China=0      D2: China=1      Num1: 5401      Num2: 873       Num_com: 0      sg_K: 131      sg_P = 259.391

(27.493999601133, <<T: Density of connectivity; D: WA=0 AND China=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 27.494      D1: WA=0 AND China=0      D2: China=1 AND IL=0      Num1: 5392      Num2: 861       Num_com: 0      sg_K: 120      sg_P = 259.108

(27.445354736331346, <<T: Density of connectivity; D: Singapore=0 AND China=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 27.445      D1: Singapore=0 AND China=0      D2: China=1 AND IL=0      Num1: 5401      Num2: 861       Num_com: 0      sg_K: 117      sg_P = 254.646

(27.41156889314796, <<T: Density of connectivity; D: China=0 AND Spain=0>>, <<T: Density of connectivity; D: China=1 AND IL=0>>)
SI = 27.412      D1: China=0 AND Spain=0      D2: China=1 AND IL=0      Num1: 5476      Num2: 861       Num_com: 0      sg_K: 120      sg_P = 258.847

(27.325984796134517, <<T: Density of connectivity; D: China=0 AND Australia=0>>, <<T: Density of connectivity; D: China=1>>)
SI = 27.326      D1: China=0 AND Australia=0      D2: China=1      Num1: 5359      Num2: 873       Num_com: 0      sg_K: 130      sg_P = 256.707

-------------------------------------------------------------------
iteration3
(29.810623872816947, <<T: Density of connectivity; D: CA=1>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 29.811      D1: CA=1      D2: CA=0 AND WA=1      Num1: 888      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.726

(26.236925186178347, <<T: Density of connectivity; D: WA=0>>, <<T: Density of connectivity; D: WA=1>>)
SI = 26.237      D1: WA=0      D2: WA=1      Num1: 6254      Num2: 218       Num_com: 0      sg_K: 182      sg_P = 97.776

(24.8967209042175, <<T: Density of connectivity; D: CA=1 AND TX=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.897      D1: CA=1 AND TX=0      D2: CA=0 AND WA=1      Num1: 876      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.568

(24.868197329028295, <<T: Density of connectivity; D: CA=1 AND Australia=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.868      D1: CA=1 AND Australia=0      D2: CA=0 AND WA=1      Num1: 877      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.581

(24.867591925953096, <<T: Density of connectivity; D: CA=1 AND IN=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.868      D1: CA=1 AND IN=0      D2: CA=0 AND WA=1      Num1: 877      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.581

(24.838504966651225, <<T: Density of connectivity; D: CA=1 AND Israel=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.839      D1: CA=1 AND Israel=0      D2: CA=0 AND WA=1      Num1: 878      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.594

(24.781059513326998, <<T: Density of connectivity; D: CA=1 AND AZ=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.781      D1: CA=1 AND AZ=0      D2: CA=0 AND WA=1      Num1: 880      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.620

(24.78045672278545, <<T: Density of connectivity; D: CA=1 AND Switzerland=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.780      D1: CA=1 AND Switzerland=0      D2: CA=0 AND WA=1      Num1: 880      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.621

(24.75209717165088, <<T: Density of connectivity; D: CA=1 AND Netherlands=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.752      D1: CA=1 AND Netherlands=0      D2: CA=0 AND WA=1      Num1: 881      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.634

(24.751495248006467, <<T: Density of connectivity; D: CA=1 AND Spain=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.751      D1: CA=1 AND Spain=0      D2: CA=0 AND WA=1      Num1: 881      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.634

(24.751495248006467, <<T: Density of connectivity; D: Japan=0 AND CA=1>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.751      D1: Japan=0 AND CA=1      D2: CA=0 AND WA=1      Num1: 881      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.634

(24.751495248006467, <<T: Density of connectivity; D: CA=1 AND United Kingdom=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.751      D1: CA=1 AND United Kingdom=0      D2: CA=0 AND WA=1      Num1: 881      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.634

(24.722575411668085, <<T: Density of connectivity; D: CA=1 AND WI=0>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.723      D1: CA=1 AND WI=0      D2: CA=0 AND WA=1      Num1: 882      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.647

(24.722575411668085, <<T: Density of connectivity; D: Italy=0 AND CA=1>>, <<T: Density of connectivity; D: CA=0 AND WA=1>>)
SI = 24.723      D1: Italy=0 AND CA=1      D2: CA=0 AND WA=1      Num1: 882      Num2: 184       Num_com: 0      sg_K: 55      sg_P = 11.647

