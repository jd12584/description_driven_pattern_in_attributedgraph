import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import os

import time
import pickle

from visualisation import *
from get_graph_and_attributes import *

Beamwidths = [1,2,3,4,7]
# OuterBeamwidths = [10]
# InnerBeamwidths = [10]

# G, data, name = from_lastfm(100)
G, data, name = from_facebook100('Reed98.mat')
ud = True
name = 'bi_'+ name
total_A = nx.adjacency_matrix(G)

cwd = os.getcwd()
file = os.path.join(cwd, *['results', name + '_nocons_varyingbw.pkl'])
# file1 = os.path.join(cwd, *['results', name + '_nocons_varyingOB10_IB5.pkl'])
# file2 = os.path.join(cwd, *['results', name + '_nocosns_varyingIB10_OB5.pkl'])

# Initial background distribution
bd_graph = BGDistr(total_A, datasource='custom')
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(iterations=1000,verbose=True,undirected=ud)
lambda_dict = {}

target = psx.GTarget(G, x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict, ud)
searchspace = psx.createSelectors(data)
print(searchspace)
print(len(searchspace))

Search_time = []
Results = []
# investigating the impacts of beamWidth
for w1 in Beamwidths:
    Search_time.append([])
    Results.append([])
    for w2 in Beamwidths:

        task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[w2,w1],depth=2,qf=psx.graph_target.SubjectiveInterestingness())

        search_time_s = time.time()
        result = psx.BeamSearch(beamWidth=10).execute_in_bicases(task)
        # result = psx.BeamSearch(beamWidth=20).execute_in_bicases_constraints(task)
        Search_time[-1].append(time.time() - search_time_s)
        Results[-1].append(result)

        # save key variables
        Obj = (Beamwidths,Search_time,Results)
        f = open(file,'wb')
        pickle.dump(Obj, f)
        f.close()

        f = open(file,'rb')
        Obj = pickle.load(f)
        f.close()
        print(Obj)

bw, time, results = Obj
i = 0
for i in range(len(bw)):
    for j in range(len(bw)):
        print('------------------------------------------------\n')
        print('when x1 = '+ str(bw[i]) + '  x2 = ' + str(bw[j]) + '\n')
        print(results[i][j])
print(bw)
print(time)


# # investigating the impacts of innerbeamwidth
# search_time = []
# Result = []
# for w2 in InnerBeamwidths:
#     # The first search
#     #
#     target = psx.GTarget(G, x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict, ud)
#
#     searchspace = psx.createSelectors(data)
#     print(searchspace)
#     print(len(searchspace))
#
#     task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[w2,5],depth=2,qf=psx.graph_target.SubjectiveInterestingness())
#
#     search_time_s = time.time()
#     result = psx.BeamSearch(beamWidth=10).execute_in_bicases(task)
#     # result = psx.BeamSearch(beamWidth=20).execute_in_bicases_constraints(task)
#     search_time.append(time.time() - search_time_s)
#
#     Result.append(result)
#     print(result)
#
#     # save key variables
#     Obj = (InnerBeamwidths,search_time,Result)
#     f = open(file2,'wb')
#     pickle.dump(Obj, f)
#     f.close()
#
#     f = open(file2,'rb')
#     Obj = pickle.load(f)
#     f.close()
#     print(Obj)
