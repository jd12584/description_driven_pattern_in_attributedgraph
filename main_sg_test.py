import numpy as np
import pandas as pd
import networkx as nx
import pysubgroupx as psx
import scipy

import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from scipy import optimize

from sklearn.preprocessing import MultiLabelBinarizer

from maxent.baseclass.optimization import *
from maxent.baseclass.MRDbase import *

from heapq import nlargest

import pickle

import time
import os

from visualisation import *
from get_graph_and_attributes import *



# -----------------------------------------------------------------------------
# Get a graph and the user-attributes data frame from a particular dataset
# -----------------------------------------------------------------------------

dataset = input('Which dataset? \n \
The synthetic graph(Type: s), Delicious(Type d), Lastfm(Type l) \
Citeseer(Type c), Caltech36(Type ct), Reed98(Type r), Germany(Type g)')

if dataset == "s":
    G, data, name = from_old_synthesized_data()
    ud=False
elif dataset == "d":
    G, data, name = from_delicious(100)
    ud=True
elif dataset == "l":
    G, data, name = from_lastfm()
    ud=True
elif dataset == "c":
    G, data, name = from_citeseer()
    ud=False
elif dataset == "ct":
    G, data, name = from_facebook100('Caltech36.mat')
    ud=True
elif dataset == "r":
    G, data, name = from_facebook100('Reed98.mat')
    ud=True
elif dataset == "g":
    G, data, name = from_germany()
    ud=True
else:
    print("Invalid option.")

# adjacency matrix

total_A = nx.adjacency_matrix(G)

# -----------------------------------------------------------------------------
# Initial background distribution
# -----------------------------------------------------------------------------

bd_graph = BGDistr(total_A, datasource='custom')
bg_time_s = time.time()
x_rows, x_columns,rowbeans_index,colbeans_index,_,_ = bd_graph.compute_lambdas_in_a_cooler_way(iterations=1000,verbose=True,undirected=ud)
bg_time =  time.time() - bg_time_s

lambda_dict = {}

Result = []


# -----------------------------------------------------------------------------
# The first Search
# -----------------------------------------------------------------------------

target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)
# print(nx.adjacency_matrix(target.graph))

searchspace = psx.createSelectors(data)
print(searchspace)

task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=2,qf=psx.SubjectiveInterestingness())


search_time = []
search_time_s = time.time()
result = psx.BeamSearch(beamWidth=10).execute(task)
search_time_e = time.time()

search_time.append(search_time_e - search_time_s)

Result.append(result)

total_A = nx.adjacency_matrix(G)



def Connecting_Prob(sg_nodes,sg_x_rows,sg_x_cols, sg_n,lambda_dict):
    '''Computing the current connecting probability of edges within the subgroup'''
    sg_P = 0

    odds_M = sg_x_rows[:, None] + sg_x_cols
    P_M = np.exp(odds_M)/(1 + np.exp(odds_M))
    np.fill_diagonal(P_M, 0.)


    for i in lambda_dict.keys():

        com_members_ids = []

        com_members = [j for j in lambda_dict[i] if j in sg_nodes]
        num_com_members = len(com_members)

        if com_members != []:

            com_members_ids = [sg_nodes.index(j) for j in com_members]

            row = np.asarray(com_members_ids*num_com_members)
            col = np.repeat(com_members_ids, num_com_members)

            value = [i]*(num_com_members**2)

            lambda_M = csr_matrix((value,(row,col)), shape=(sg_n, sg_n)).toarray()

            odds_M = P_M * np.exp(lambda_M)

            P_M = odds_M / (1. - P_M + odds_M)
        else:

            P_M = P_M

    return P_M


def f_constraint(x, P_M, sg_n, sg_K):
    '''Computing the lambda'''
    update_lambda_M = np.exp(x)*np.ones((sg_n,sg_n))
    update_odds_M = P_M * update_lambda_M

    P_M = update_odds_M / (1. - P_M + update_odds_M)

    sg_P = np.sum(P_M)

    return sg_P-sg_K


# -----------------------------------------------------------------------------
# The sequel
# -----------------------------------------------------------------------------

num_it = 5;
end_base = 50.

for it in range(num_it):

    _, sg = result[0]

    sg_nodes, sg_x_rows, sg_x_cols = sg.get_lambdas(data, weightingAttribute=None)
    sg_n = len(sg_nodes)

    P_M = Connecting_Prob(sg_nodes, sg_x_rows, sg_x_cols, sg_n, lambda_dict)

    sg_A = nx.adjacency_matrix(G,sg_nodes)
    sg_K = sg_A.count_nonzero()

    if sg_K != 0:
        new_lambda = optimize.brentq(f_constraint, -end_base, end_base, args = (P_M, sg_n, sg_K))
        end_base += 20.
    else:
        new_lambda = optimize.newton(f_constraint, 5., args = (P_M, sg_n, sg_K))

    if new_lambda != 0.:
        lambda_dict[new_lambda] = sg_nodes

    target = psx.GTarget(G,x_rows, x_columns, rowbeans_index, colbeans_index, lambda_dict)

    task = psx.SubgroupDiscoveryTask(data,target,searchspace,resultSetSize=[10],depth=2,qf=psx.SubjectiveInterestingness())

    result = psx.BeamSearch(beamWidth=10).execute(task)
    Result.append(result)

    print(lambda_dict)


for i in range(len(Result)):
    print('----------------------------------------')
    print('iteration' + str(i))
    print(Result[i])
# # #
# #
# # -----------------------------------------------------------------------------
# # Save some key variables
# # -----------------------------------------------------------------------------
# cwd = os.getcwd()
# file = os.path.join(cwd, *['results', name +'_results.pkl'])
#
# Obj = (x_rows, x_columns,rowbeans_index,colbeans_index,lambda_dict,bg_time, search_time,Result)
# f = open(file,'wb')
# pickle.dump(Obj, f)
# f.close()

# f = open(file,'rb')
# Obj = pickle.load(f)
# f.close()
#
# for i in range(len(Obj[-1])):
#     print('----------------------------------------')
#     print('iteration' + str(i))
#     print(Obj[-1][i])
# #
#
# -----------------------------------------------------------------------------
# Visualization
# -----------------------------------------------------------------------------

SI, sg1, sg2 = Obj[-1][0][0]

sg1_instances = sg1.subgroupDescription.covers(data)
sg1_nodes = np.where(sg1_instances)[0]

sg2_instances = sg2.subgroupDescription.covers(data)
sg2_nodes = np.where(sg2_instances)[0]

title = 'SI = ' + '{:.03f}'.format(SI) + '      D1: ' + str(sg1.subgroupDescription)+ '      D2: ' + str(sg2.subgroupDescription)


cwd = os.getcwd()
file = os.path.join(cwd, *['results',name,'00.pdf'])
#
if dataset == "s":
    indicate_cluster_in_smallNetwork(G, list(sg_nodes),file, title)
# elif dataset == "g":
#     indicate_cluster_in_map(G, list(sg_nodes), file, title)
else:
    indicate_cluster_in_realNetwork_u(G, list(sg_nodes),file, title)
